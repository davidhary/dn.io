# The MIT License
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE.

## License Dependencies

[stateless -- Dot Net State Machine -- Apache License 2.0, Janualy 2004](https://github.com/dotnet-state-machine/stateless)  
[File System Watcher Pure Chaos -- The Code Project Open License (CPOL) 1.02] (https://www.codeproject.com/info/cpol10.aspx)  
[Text file data set -- The Code Project Open License (CPOL) 1.02] (https://www.codeproject.com/info/cpol10.aspx)  
[ODS Read Write -- The Code Project Open License (CPOL) 1.02] (https://www.codeproject.com/info/cpol10.aspx)  
[dot net zip -- Microsoft Public License (Ms-PL)] (https://github.com/DinoChiesa/DotNetZip/blob/master/License.txt)