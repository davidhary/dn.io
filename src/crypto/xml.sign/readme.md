# About

isr.IO.XmlSign is a .Net library supporting XML signature.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

isr.IO.XmlSign is released as open source under the MIT license.
Bug reports and contributions are welcome at the [IO Repository].

[IO Repository]: https://bitbucket.org/davidhary/dn.io

