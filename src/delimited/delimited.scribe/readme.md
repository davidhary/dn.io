# About

isr.IO.Delimited.Scribe is a .Net library supporting comma-separated-values I/O.

# How to Use

```
TBD
```

# Key Features

* Writes and reads delimited (CSV) files.

# Main Types

The main types provided by this library are:

* _DelimitedFileBase_ A base class for the Delimited file scribe (writer and reader).
* _DelimitedReader_ A reader of a delimited file.
* _DelimitedWriter_ A writer of a delimited file.

# Feedback

isr.IO.Delimited.Scribe is released as open source under the MIT license.
Bug reports and contributions are welcome at the [IO Repository].

[IO Repository]: https://bitbucket.org/davidhary/dn.io

