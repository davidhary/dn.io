# About

isr.IO.NetList is a .Net library supporting Net List formatted I/O.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

isr.IO.NetList is released as open source under the MIT license.
Bug reports and contributions are welcome at the [IO Repository].

[IO Repository]: https://bitbucket.org/davidhary/dn.io

