using System;
using System.Collections.Generic;
using System.Diagnostics;

using FluentAssertions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.IO.OfficeReader.MSTest
{

    /// <summary>
    /// Contains all unit tests for the <see cref="isr.IO.Office.ExcelReader">Excel Reader</see>
    /// class.
    /// </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-06-21 </para>
    /// </remarks>
    [TestClass()]
    public class ExcelReaderTest
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " WORKBOOK SETTINGS "

        /// <summary> Gets the full pathname of the file. </summary>
        /// <value> The full pathname of the file. </value>
        internal static string FullFileName => System.IO.Path.Combine( System.AppDomain.CurrentDomain.BaseDirectory, FileName );

        /// <summary> Gets or sets the filename of the file. </summary>
        /// <value> The name of the file. </value>
        public static string FileName => "\\OfficeReader\\Products.xlsx";

        /// <summary> Gets or sets a list of names of the sheets. </summary>
        /// <value> A list of names of the sheets. </value>
        public static string SheetNames => "'Categories','Products','Sales','SalesDetails'";

        /// <summary> The sheet name values. </summary>
        private static System.Collections.ObjectModel.Collection<string> _SheetNameValues;

        /// <summary> Gets the sheet name values. </summary>
        /// <value> The sheet name values. </value>
        public static System.Collections.ObjectModel.Collection<string> SheetNameValues
        {
            get {
                if ( ExcelReaderTest._SheetNameValues is null )
                {
                    ExcelReaderTest._SheetNameValues = new System.Collections.ObjectModel.Collection<string>( new List<string>( SheetNames.Split( ',' ) ) );
                }

                return ExcelReaderTest._SheetNameValues;
            }
        }

        /// <summary> Gets or sets the category query. </summary>
        /// <value> The category query. </value>
        public static string CategoryQuery => "Select [CategoryName] from [Categories] Where ID=1;";

        /// <summary> Gets or sets the category result. </summary>
        /// <value> The category result. </value>
        public static string CategoryResult => "Air Filters";

        /// <summary> Gets or sets the number of categories rows. </summary>
        /// <value> The number of categories rows. </value>
        public static int CategoriesRowCount => 6;

        #endregion


        #region " EXCEL READER : Requires installing the Access Redistributable for Ace Provider "

        /// <summary> Reads Workbook into tables. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestMethod()]
        [Description( "Reads Workbook into tables" )]
        public void ReadXlsxJetWorkbookReaderTest()
        {
            bool is64BitProcess = IntPtr.Size == 8;
            // the Jet provider requires a 32 bit process.
            if ( is64BitProcess )
                return;
            using var reader = new Office.ExcelReader();
            Office.XlsxOdbcImport.PreferredProvider = Office.XlsxOdbcImport.JetProvider4;
            bool success = reader.ReadWorkbook( ExcelReaderTest.FullFileName );
            _ = success.Should().BeTrue( $"workbook is read from {reader.FullFileName}" );
        }

        #endregion

    }
}
