using System;
using System.Configuration;

using FluentAssertions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.IO.Profile.MSTest
{

    /// <summary>
    /// Contains all unit tests for the <see cref="isr.IO.Profile.Scribe">Profile Writer and Reader</see>
    /// class.
    /// </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-06-21 </para>
    /// </remarks>
    [TestClass()]
    public class ProfileScribeTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.Out.WriteLine( testContext.FullyQualifiedTestClassName );
                _Scribe = new Scribe( System.Reflection.Assembly.GetAssembly( typeof( ProfileScribeTests ) ) );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            _Scribe = null;
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " SHARED SCRIBE INSTANCE "

        /// <summary> The scribe. </summary>
        private static Scribe _Scribe;

        /// <summary> Delete profile. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        private static void DeleteProfileFile()
        {
            if ( _Scribe.Exists() )
            {
                // delete the existing file.
                System.IO.File.Delete( _Scribe.FullFileName );
            }
            // the file is created on the first write.
        }

        #endregion

        #region " READER "

        private static void AssertReadWrite()
        {

            string sectionName = nameof( ProfileScribeTests );
            string stringSettingsName = "String Value";
            string stringExpectedValue = "This is a string value";
            ProfileScribeTests._Scribe.SetValue( sectionName, stringSettingsName, stringExpectedValue );

            string dateSettingsName = "Date Value";
            DateTime dateExpectedValue = DateTime.Now.Date;
            ProfileScribeTests._Scribe.SetValue( sectionName, dateSettingsName, dateExpectedValue.ToShortDateString() );

            string dateTimeFormat = "yyyy-MM-dd HH:mm:ss.fffffff";
            string dateTimeSettingsName = "Date Time Value";
            DateTime dateTimeExpectedValue = DateTime.Now;
            ProfileScribeTests._Scribe.SetValue( sectionName, dateTimeSettingsName, dateTimeExpectedValue.ToString( dateTimeFormat ) );

            string doublePositiveSettingsName = "double Positive Value";
            double doublePositiveExpectedValue = 7523.65087890625;
            ProfileScribeTests._Scribe.SetValue( sectionName, doublePositiveSettingsName, doublePositiveExpectedValue.ToString() );

            string doubleNegativeSettingsName = "double Negative Value";
            double doubleNegativeExpectedValue = -7523.65087890625;
            ProfileScribeTests._Scribe.SetValue( sectionName, doubleNegativeSettingsName, doubleNegativeExpectedValue.ToString() );

            string intPositiveSettingsName = "int Positive Value";
            int intPositiveExpectedValue = 1;
            ProfileScribeTests._Scribe.SetValue( sectionName, intPositiveSettingsName, intPositiveExpectedValue.ToString() );

            string intNegativeSettingsName = "int Negative Value";
            int intNegativeExpectedValue = -1;
            ProfileScribeTests._Scribe.SetValue( sectionName, intNegativeSettingsName, intNegativeExpectedValue.ToString() );

            string fileNameSettingsName = "File Name";
            string fileNameExpectedValue = System.Reflection.Assembly.GetAssembly( typeof( ProfileScribeTests ) ).Location;
            ProfileScribeTests._Scribe.SetValue( sectionName, fileNameSettingsName, fileNameExpectedValue );

            string locationSettingsName = "Location Value";
            string locationExpectedValue = "32.2812N, 83.4209W";
            ProfileScribeTests._Scribe.SetValue( sectionName, locationSettingsName, locationExpectedValue );

            string boolTrueSettingsName = "bool True Value";
            bool boolTrueExpectedValue = true;
            ProfileScribeTests._Scribe.SetValue( sectionName, boolTrueSettingsName, boolTrueExpectedValue.ToString() );

            string boolFalseSettingsName = "bool False Value";
            bool boolFalseExpectedValue = false;
            ProfileScribeTests._Scribe.SetValue( sectionName, boolFalseSettingsName, boolFalseExpectedValue.ToString() );

            ProfileScribeTests._Scribe.Write();

            ProfileScribeTests._Scribe.Read();

            _ = ProfileScribeTests._Scribe.GetValue( sectionName, stringSettingsName, string.Empty ).Should().Be( stringExpectedValue,
                $"scriber should read [{sectionName}].[{stringSettingsName}]={stringExpectedValue} from '{_Scribe.FullFileName}'" );

            _ = ProfileScribeTests._Scribe.GetValue( sectionName, dateSettingsName, DateTime.MinValue ).Date.Should().Be( dateExpectedValue,
                $"scriber should read [{sectionName}].[{dateSettingsName}]={dateExpectedValue} from '{_Scribe.FullFileName}'" );

            _ = ProfileScribeTests._Scribe.GetValue( sectionName, dateTimeSettingsName, DateTime.MinValue ).Should().Be( dateTimeExpectedValue,
                $"scriber should read [{sectionName}].[{dateTimeSettingsName}]={dateTimeExpectedValue.ToString( dateTimeFormat )} from '{_Scribe.FullFileName}'" );

            _ = ProfileScribeTests._Scribe.GetValue( sectionName, doublePositiveSettingsName, ( double ) 0 ).Should().Be( doublePositiveExpectedValue,
                $"scriber should read [{sectionName}].[{doublePositiveSettingsName}]={doublePositiveExpectedValue} from '{_Scribe.FullFileName}'" );

            _ = ProfileScribeTests._Scribe.GetValue( sectionName, doubleNegativeSettingsName, ( double ) 0 ).Should().Be( doubleNegativeExpectedValue,
                $"scriber should read [{sectionName}].[{doubleNegativeSettingsName}]={doubleNegativeExpectedValue} from '{_Scribe.FullFileName}'" );

            _ = ProfileScribeTests._Scribe.GetValue( sectionName, intPositiveSettingsName, 0 ).Should().Be( intPositiveExpectedValue,
                $"scriber should read [{sectionName}].[{intPositiveSettingsName}]={intPositiveExpectedValue} from '{_Scribe.FullFileName}'" );

            _ = ProfileScribeTests._Scribe.GetValue( sectionName, intNegativeSettingsName, 0 ).Should().Be( intNegativeExpectedValue,
                $"scriber should read [{sectionName}].[{intNegativeSettingsName}]={intNegativeExpectedValue} from '{_Scribe.FullFileName}'" );

            _ = ProfileScribeTests._Scribe.GetValue( sectionName, fileNameSettingsName, string.Empty ).Should().Be( fileNameExpectedValue,
                $"scriber should read [{sectionName}].[{fileNameSettingsName}]={fileNameExpectedValue} from '{_Scribe.FullFileName}'" );

            _ = ProfileScribeTests._Scribe.GetValue( sectionName, locationSettingsName, string.Empty ).Should().Be( locationExpectedValue,
                $"scriber should read [{sectionName}].[{locationSettingsName}]={locationExpectedValue} from '{_Scribe.FullFileName}'" );

            _ = ProfileScribeTests._Scribe.GetValue( sectionName, boolTrueSettingsName, false ).Should().Be( boolTrueExpectedValue,
                $"scriber should read [{sectionName}].[{boolTrueSettingsName}]={boolTrueExpectedValue} from '{_Scribe.FullFileName}'" );

            _ = ProfileScribeTests._Scribe.GetValue( sectionName, boolFalseSettingsName, true ).Should().Be( boolFalseExpectedValue,
                $"scriber should read [{sectionName}].[{boolFalseSettingsName}]={boolFalseExpectedValue} from '{_Scribe.FullFileName}'" );
        }

        /// <summary> Reads Workbook into tables. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestMethod()]
        [Description( "Tests writing and reading a private profile string" )]
        public void ShouldWriteAndReadProfileValues()
        {
            if ( _Scribe.Exists() )
            {
                System.IO.File.Delete( _Scribe.FullFileName );
                _Scribe.Clear();
            }
            // write and read pass one.
            ProfileScribeTests.AssertReadWrite();

            // write and read pass two to verify overrides..
            ProfileScribeTests.AssertReadWrite();

        }

        #endregion

    }
}
