using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace isr.IO.Profile
{
    /// <summary>
    /// A class for reading values by section and key names from a standard ".ini" initialization file.
    /// </summary>
    /// <remarks>
    /// Section and key names are not case-sensitive. Values are loaded into a hash table for fast access.
    /// Use <see cref="GetAllValues"/> to read multiple values that share the same section and key names.
    /// Sections in the initialization file must have the following form:
    /// <code>
    ///     ; comment line
    ///     [section]
    ///     key=value
    /// </code>
    /// </remarks>
    public class Scribe : IDisposable
    {

        #region  " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor using the calling assembly. </summary>
        /// <remarks>   David, 2021-08-07. </remarks>
        public Scribe() : this( System.Reflection.Assembly.GetCallingAssembly() )
        {
        }

        /// <summary>   Initializes a new instance of the <see cref="Scribe"/> class. </summary>
        /// <remarks>   David, 2021-08-06. </remarks>
        /// <param name="callingAssembly">  The calling assembly. </param>
        /// <param name="commentDelimiter"> (Optional) The comment delimiter string (default value is ";
        ///                                 "). </param>
        /// <param name="scope">            (Optional) The scope. </param>
        public Scribe( Assembly callingAssembly, string commentDelimiter = ";", ProfileScope scope = ProfileScope.ApplicationScope )
        {
            this.AppContextSettingsFullFileName = Scribe.BuildAppContextProfileFullFileName( callingAssembly );
            this.AppDataSettingsFullFileName = Scribe.BuildApplicationDataProfileFullFileName( callingAssembly, scope );
            this.Scope = scope;
            this.CommentDelimiter = commentDelimiter;
            this.FullFileName = this.SelectFileName();
            this.Read( this.FullFileName );
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// https://docs.microsoft.com/en-us/dotnet/standard/garbage-collection/implementing-dispose
        /// </remarks>
        public void Dispose()
        {
            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            // The Dispose method performs all object cleanup, so the garbage collector no 
            // longer needs to call the objects' Object.Finalize override. Therefore, the call 
            // to the SuppressFinalize method prevents the garbage collector from running the finalizer.
            // If the type has no finalizer, the call to GC.SuppressFinalize has no effect. 
            // Note that the actual cleanup is performed by the Dispose(bool) method overload.
            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Gets the dispose status sentinel of the base class.  This applies to the derived class
        /// provided proper implementation.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:ProfileScribe" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        protected virtual void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;

            try
            {
                if ( disposing )
                {
                    this._Dictionary?.Clear();
                }
                // Assign large managed object references to null to make them more likely 
                // to be unreachable. This releases them faster than if they were reclaimed 
                // non-deterministically, and this is usually done outside of the conditional block.
                this._Dictionary = null;
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/destructors
        /// </remarks>
        ~Scribe()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }

        #endregion

        #region " CONTENTS "

        /// <summary>
        /// The comment delimiter string (default value is ";").
        /// </summary>
        public string CommentDelimiter { get; set; }

        /// <summary>   (Immutable) the dictionary. </summary>
        /// <remarks>
        /// "[section]key"   -> "value1"
        /// "[section]key~2" -> "value2"
        /// "[section]key~3" -> "value3"
        /// </remarks>
        private Dictionary<string, string> _Dictionary = new();

        private readonly List<string> _SectionNames = new();

        /// <summary>   Enumerates section names in this collection. </summary>
        /// <remarks>   David, 2021-08-07. </remarks>
        /// <returns>
        /// An enumerator that allows foreach to be used to process section names in this collection.
        /// </returns>
        public IEnumerable<string> SectionNames()
        {
            return this._SectionNames;
        }

        /// <summary>   Clears this object to its blank/initial state. </summary>
        /// <remarks>   David, 2021-08-06. </remarks>
        public void Clear()
        {
            this._Dictionary.Clear();
        }

        /// <summary>   Reads the file. </summary>
        /// <remarks>
        /// Section and key names are not case-sensitive. Values are loaded into a hash table for fast
        /// access. Use <see cref="GetAllValues"/> to read multiple values that share the same section
        /// and key names. Sections in the initialization file must have the following form:
        /// <code>
        ///     ; comment line
        ///     [section]
        ///     key=value
        /// </code>
        /// </remarks>
        /// <param name="fullFileName"> The profile (INI, initialization) file path. </param>
        private void Read( string fullFileName )
        {
            this._Dictionary.Clear();
            this._SectionNames.Clear();
            if ( !File.Exists( fullFileName ) ) return; // nothing to read.
            using StreamReader sr = new( fullFileName );
            string line, sectionName = string.Empty;
            while ( !sr.EndOfStream )
            {
                line = sr.ReadLine();
                if ( string.IsNullOrWhiteSpace( line ) ) continue; // empty line

                line = line.Trim();
                if ( line.Length == 0 ) continue;  // empty line

                if ( !String.IsNullOrEmpty( this.CommentDelimiter ) && line.StartsWith( this.CommentDelimiter ) )
                    continue;  // comment

                if ( line.StartsWith( "[" ) && line.Contains( "]" ) )  // [section]
                {
                    int index = line.IndexOf( ']' );
                    sectionName = line.Substring( 1, index - 1 ).Trim();

                    if ( !this._SectionNames.Contains( sectionName ) )
                        this._SectionNames.Add( sectionName );
                    continue;
                }

                if ( line.Contains( "=" ) )  // key=value
                {
                    int index = line.IndexOf( '=' );
                    string key = line.Substring( 0, index ).Trim();
                    string val = line.Substring( index + 1 ).Trim();
                    string key2 = $"[{sectionName}]{key}".ToLower();

                    if ( val.StartsWith( "\"" ) && val.EndsWith( "\"" ) )  // strip quotes
                        val = val.Substring( 1, val.Length - 2 );

                    if ( this._Dictionary.ContainsKey( key2 ) )  // multiple values can share the same key
                    {
                        index = 1;
                        string key3;
                        while ( true )
                        {
                            key3 = $"{key2}~{++index}";
                            if ( !this._Dictionary.ContainsKey( key3 ) )
                            {
                                this._Dictionary.Add( key3, val );
                                break;
                            }
                        }
                    }
                    else
                    {
                        this._Dictionary.Add( key2, val );
                    }
                }
            }
        }

        /// <summary>   Reads the file. </summary>
        /// <remarks>
        /// Section and key names are not case-sensitive. Values are loaded into a hash table for fast
        /// access. Use <see cref="GetAllValues"/> to read multiple values that share the same section
        /// and key names. Sections in the initialization file must have the following form:
        /// <code>
        ///     ; comment line
        ///     [section]
        ///     key=value
        /// </code>
        /// </remarks>
        public void Read()
        {
            // if the application data file does not exist read from the application context file. 
            string fileName = File.Exists( this.AppDataSettingsFullFileName ) ? this.AppDataSettingsFullFileName : this.AppContextSettingsFullFileName;
            if ( File.Exists( fileName ) )
            {
                this.Read( fileName );
            }
        }

        /// <summary>   Restore profile from the application context file name. </summary>
        /// <remarks>   David, 2021-07-27. </remarks>
        public void Restore()
        {
            string fileName = this.AppContextSettingsFullFileName;
            if ( File.Exists( fileName ) )
            {
                this.Read( fileName );
            }
        }

        /// <summary>   Writes the file. </summary>
        /// <remarks>
        /// Section and key names are not case-sensitive. Values are loaded into a hash table for fast
        /// access. Use <see cref="GetAllValues"/> to read multiple values that share the same section
        /// and key names. Sections in the initialization file must have the following form:
        /// <code>
        ///     ; comment line
        ///     [section]
        ///     key=value
        /// </code>
        /// </remarks>
        public void Write()
        {
            this.Write( this.FullFileName );
        }

        /// <summary>   Writes the file. </summary>
        /// <remarks>
        /// Section and key names are not case-sensitive. Values are loaded into a hash table for fast
        /// access. Use <see cref="GetAllValues"/> to read multiple values that share the same section
        /// and key names. Sections in the initialization file must have the following form:
        /// <code>
        ///     ; comment line
        ///     [section]
        ///     key=value
        /// </code>
        /// </remarks>
        /// <param name="fullFileName"> The profile (INI, initialization) file path. </param>
        private void Write( string fullFileName )
        {
            if ( File.Exists( fullFileName ) )
            {
                File.Delete( fullFileName );
            }
            else
            {
                string dirPath = Path.GetDirectoryName( fullFileName );
                if ( !Directory.Exists( dirPath ) )
                    _ = Directory.CreateDirectory( dirPath );
            }
            using StreamWriter writer = new( fullFileName );
            foreach ( var sectionName in this._SectionNames )
            {
                _ = this.WriteSection( writer, sectionName );
            }
        }

        /// <summary>   Writes a section. </summary>
        /// <remarks>   David, 2021-08-06. </remarks>
        /// <param name="writer">   The writer. </param>
        /// <param name="sectionName">  The section name. </param>
        /// <returns>   An int. </returns>
        private int WriteSection( StreamWriter writer, string sectionName )
        {
            int recordCount = 0;
            writer.WriteLine( sectionName );
            foreach ( var item in this._Dictionary )
            {
                if ( item.Key.StartsWith( sectionName ) )
                {
                    string key = item.Key.Substring( sectionName.Length );
                    int tildeIndex = key.IndexOf( '~' );
                    if ( tildeIndex > 0 )
                        key = key.Substring( 0, tildeIndex );
                    writer.WriteLine( $"{key}={item.Value}" );
                    recordCount += 1;
                }
            }
            return recordCount;
        }

        #endregion

        #region " GET / SET SECTIONS ITEMS '"

        /// <summary>   Attempts to get value. </summary>
        /// <remarks>   David, 2021-08-06. </remarks>
        /// <param name="sectionName">  The section name. </param>
        /// <param name="key">          The key. </param>
        /// <param name="value">        [out] The value. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        private bool TryGetValue( string sectionName, string key, out string value )
        {
            string key2 = sectionName.StartsWith( "[" ) ? $"{sectionName}{key}" : $"[{sectionName}]{key}";
            return this._Dictionary.TryGetValue( key2.ToLower(), out value );
        }

        /// <summary>   Gets a string value by section and key names. </summary>
        /// <remarks>   David, 2021-08-06. </remarks>
        /// <param name="sectionName">  The section name. </param>
        /// <param name="key">          The key. </param>
        /// <param name="defaultValue"> (Optional) The default value. </param>
        /// <returns>   The value. </returns>
        /// <seealso cref="GetAllValues"/>
        public string GetValue( string sectionName, string key, string defaultValue = "" )
        {
            return !this.TryGetValue( sectionName, key, out string value ) ? defaultValue : value;
        }

        /// <summary>   Gets a string value by section and key names. </summary>
        /// <param name="sectionName">  The section name. </param>
        /// <param name="key">          The key. </param>
        /// <returns>   The value. </returns>
        public string this[string sectionName, string key] => this.GetValue( sectionName, key );

        /// <summary>   Gets an <see cref="byte"/> value by section and key names. </summary>
        /// <remarks>   David, 2021-08-06. </remarks>
        /// <param name="sectionName">  The section name. </param>
        /// <param name="key">          The key. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <param name="minValue">     (Optional) minimum value to be enforced. </param>
        /// <param name="maxValue">     (Optional) maximum value to be enforced. </param>
        /// <returns>   The value. </returns>
        public byte GetValue( string sectionName, string key, byte defaultValue, byte minValue = byte.MinValue, byte maxValue = byte.MaxValue )
        {
            if ( !this.TryGetValue( sectionName, key, out string stringValue ) )
                return defaultValue;

            if ( !byte.TryParse( stringValue, out byte value ) )
            {
                if ( !double.TryParse( stringValue, out double dvalue ) )
                    return defaultValue;
                value = ( byte ) dvalue;
            }

            if ( value < minValue )
                value = minValue;
            if ( value > maxValue )
                value = maxValue;
            return value;
        }

        /// <summary>   Gets an <see cref="Int16"/> value by section and key names. </summary>
        /// <remarks>   David, 2021-08-06. </remarks>
        /// <param name="sectionName">  The section name. </param>
        /// <param name="key">          The key. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <param name="minValue">     (Optional) minimum value to be enforced. </param>
        /// <param name="maxValue">     (Optional) maximum value to be enforced. </param>
        /// <returns>   The value. </returns>
        public Int16 GetValue( string sectionName, string key, Int16 defaultValue, Int16 minValue = Int16.MinValue, Int16 maxValue = Int16.MaxValue )
        {
            if ( !this.TryGetValue( sectionName, key, out string stringValue ) )
                return defaultValue;

            if ( !Int16.TryParse( stringValue, out Int16 value ) )
            {
                if ( !double.TryParse( stringValue, out double dvalue ) )
                    return defaultValue;
                value = ( Int16 ) dvalue;
            }

            if ( value < minValue )
                value = minValue;
            if ( value > maxValue )
                value = maxValue;
            return value;
        }

        /// <summary>   Gets an <see cref="Int32"/> value by section and key names. </summary>
        /// <remarks>   David, 2021-08-06. </remarks>
        /// <param name="sectionName">  The section name. </param>
        /// <param name="key">          The key. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <param name="minValue">     (Optional) minimum value to be enforced. </param>
        /// <param name="maxValue">     (Optional) maximum value to be enforced. </param>
        /// <returns>   The value. </returns>
        public Int32 GetValue( string sectionName, string key, Int32 defaultValue, Int32 minValue = Int32.MinValue, Int32 maxValue = Int32.MaxValue )
        {
            if ( !this.TryGetValue( sectionName, key, out string stringValue ) )
                return defaultValue;

            if ( !Int32.TryParse( stringValue, out Int32 value ) )
            {
                if ( !double.TryParse( stringValue, out double dvalue ) )
                    return defaultValue;
                value = ( Int32 ) dvalue;
            }

            if ( value < minValue )
                value = minValue;
            if ( value > maxValue )
                value = maxValue;
            return value;
        }

        /// <summary>   Gets an <see cref="Int64"/> value by section and key names. </summary>
        /// <remarks>   David, 2021-08-06. </remarks>
        /// <param name="sectionName">  The section name. </param>
        /// <param name="key">          The key. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <param name="minValue">     (Optional) minimum value to be enforced. </param>
        /// <param name="maxValue">     (Optional) maximum value to be enforced. </param>
        /// <returns>   The value. </returns>
        public Int64 GetValue( string sectionName, string key, Int64 defaultValue, Int64 minValue = Int64.MinValue, Int64 maxValue = Int64.MaxValue )
        {
            if ( !this.TryGetValue( sectionName, key, out string stringValue ) )
                return defaultValue;

            if ( !Int64.TryParse( stringValue, out Int64 value ) )
            {
                if ( !double.TryParse( stringValue, out double dvalue ) )
                    return defaultValue;
                value = ( Int64 ) dvalue;
            }

            if ( value < minValue )
                value = minValue;
            if ( value > maxValue )
                value = maxValue;
            return value;
        }

        /// <summary>   Gets a double floating-point value by section and key names. </summary>
        /// <remarks>   David, 2021-08-06. </remarks>
        /// <param name="sectionName">  The section name. </param>
        /// <param name="key">          The key. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <param name="minValue">     (Optional) minimum value to be enforced. </param>
        /// <param name="maxValue">     (Optional) maximum value to be enforced. </param>
        /// <returns>   The value. </returns>
        public double GetValue( string sectionName, string key, double defaultValue, double minValue = double.MinValue, double maxValue = double.MaxValue )
        {
            if ( !this.TryGetValue( sectionName, key, out string stringValue ) )
                return defaultValue;

            if ( !double.TryParse( stringValue, out double value ) )
                return defaultValue;

            if ( value < minValue )
                value = minValue;
            if ( value > maxValue )
                value = maxValue;
            return value;
        }

        /// <summary>   Gets a decimal floating-point value by section and key names. </summary>
        /// <remarks>   David, 2021-08-06. </remarks>
        /// <param name="sectionName">  The section name. </param>
        /// <param name="key">          The key. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <param name="minValue">     (Optional) minimum value to be enforced. </param>
        /// <param name="maxValue">     (Optional) maximum value to be enforced. </param>
        /// <returns>   The value. </returns>
        public decimal GetValue( string sectionName, string key, decimal defaultValue, decimal minValue = decimal.MinValue, decimal maxValue = decimal.MaxValue )
        {
            if ( !this.TryGetValue( sectionName, key, out string stringValue ) )
                return defaultValue;

            if ( !decimal.TryParse( stringValue, out decimal value ) )
                return defaultValue;

            if ( value < minValue )
                value = minValue;
            if ( value > maxValue )
                value = maxValue;
            return value;
        }

        /// <summary>   Gets a float floating-point value by section and key names. </summary>
        /// <remarks>   David, 2021-08-06. </remarks>
        /// <param name="sectionName">  The section name. </param>
        /// <param name="key">          The key. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <param name="minValue">     (Optional) minimum value to be enforced. </param>
        /// <param name="maxValue">     (Optional) maximum value to be enforced. </param>
        /// <returns>   The value. </returns>
        public float GetValue( string sectionName, string key, float defaultValue, float minValue = float.MinValue, float maxValue = float.MaxValue )
        {
            if ( !this.TryGetValue( sectionName, key, out string stringValue ) )
                return defaultValue;

            if ( !float.TryParse( stringValue, out float value ) )
                return defaultValue;

            if ( value < minValue )
                value = minValue;
            if ( value > maxValue )
                value = maxValue;
            return value;
        }

        /// <summary>   Gets a boolean value by section and key names. </summary>
        /// <remarks>   David, 2021-08-06. </remarks>
        /// <param name="sectionName">  The section name. </param>
        /// <param name="key">          The key. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns>   The value. </returns>
        public bool GetValue( string sectionName, string key, bool defaultValue )
        {
            return !this.TryGetValue( sectionName, key, out string stringValue )
                ? defaultValue
                : stringValue != "0" && !stringValue.StartsWith( "f", true, null );
        }

        /// <summary>   Gets data time by section and key names. </summary>
        /// <remarks>   David, 2021-08-06. </remarks>
        /// <param name="sectionName">  The section name. </param>
        /// <param name="key">          The key. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns>   The data time. </returns>
        public DateTime GetValue( string sectionName, string key, DateTime defaultValue )
        {
            return !this.TryGetValue( sectionName, key, out string stringValue )
                ? defaultValue
                : DateTime.TryParse( stringValue, out DateTime value ) ? value : defaultValue;
        }

        /// <summary>   Gets data time offset by section and key names. </summary>
        /// <remarks>   David, 2021-08-06. </remarks>
        /// <param name="sectionName">  The section name. </param>
        /// <param name="key">          The key. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns>   The data time. </returns>
        public DateTimeOffset GetValue( string sectionName, string key, DateTimeOffset defaultValue )
        {
            return !this.TryGetValue( sectionName, key, out string stringValue )
                ? defaultValue
                : DateTimeOffset.TryParse( stringValue, out DateTimeOffset value ) ? value : defaultValue;
        }

        /// <summary>   Gets an array of string values by section and key names. </summary>
        /// <remarks>   David, 2021-08-06. </remarks>
        /// <param name="sectionName">  The section name. </param>
        /// <param name="key">          The key. </param>
        /// <returns>   The array of values, or empty if none found. </returns>
        public string[] GetAllValues( string sectionName, string key )
        {
            string key2, key3;
            key2 = Scribe.BuildKey( sectionName, key );

            if ( !this._Dictionary.TryGetValue( key2, out string value ) )
                return Array.Empty<string>();

            List<string> values = new() { value };
            int index = 1;
            while ( true )
            {
                key3 = String.Format( "{0}~{1}", key2, ++index );
                if ( !this._Dictionary.TryGetValue( key3, out value ) )
                    break;
                values.Add( value );
            }

            return values.ToArray();
        }

        /// <summary>   Builds a key. </summary>
        /// <remarks>   David, 2021-08-06. </remarks>
        /// <param name="sectionName">  The section name. </param>
        /// <param name="key">          The key. </param>
        /// <returns>   A string. </returns>
        private static string BuildKey( string sectionName, string key )
        {
            return $"{Scribe.BuildSectionName( sectionName )}{key.ToLower()}";
        }

        /// <summary>   Builds section name. </summary>
        /// <remarks>   David, 2021-08-07. </remarks>
        /// <param name="sectionName">  Name of the section. </param>
        /// <returns>   A string. </returns>
        private static string BuildSectionName( string sectionName )
        {
            return (sectionName.StartsWith( "[" ) ? $"{sectionName}" : $"[{sectionName}]").ToLower();
        }

        /// <summary>
        /// Set a  dictionary value. Use <see cref="Write(string)"/> to save to the Profile (INI) file.
        /// </summary>
        /// <remarks>   David, 2021-08-06. </remarks>
        /// <param name="sectionName">  The section name. </param>
        /// <param name="key">          The key. </param>
        /// <param name="value">        The value. </param>
        public void SetValue( string sectionName, string key, string value )
        {
            sectionName = Scribe.BuildSectionName( sectionName );
            if ( !this._SectionNames.Contains( sectionName ) )
                this._SectionNames.Add( sectionName );

            string key2 = Scribe.BuildKey( sectionName, key );
            if ( this._Dictionary.ContainsKey( key2 ) )  // multiple values can share the same key
            {
                this._Dictionary[key2] = value;
            }
            else
            {
                this._Dictionary.Add( key2, value );
            }
        }

        #endregion

        #region " FILE "

        /// <summary> Builds default file name. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> A String. </returns>
        public static string BuildDefaultFileName( Assembly callingAssembly, ProfileScope scope = ProfileScope.ApplicationScope )
        {
            return Scribe.IsAppContextRestricted ? Scribe.BuildApplicationDataProfileFullFileName( callingAssembly, scope )
                                                 : Scribe.BuildAppContextProfileFullFileName( callingAssembly );
        }

        /// <summary> Holds the default extension of an INI file. </summary>
        public const string DefaultExtension = ".ini";

        /// <summary>   Gets or sets the scope. </summary>
        /// <value> The scope. </value>
        public ProfileScope Scope { get; set; }

        /// <summary>   Gets the file name suffix. </summary>
        /// <value> The file name suffix. </value>
        public string FileNameSuffix => Scribe.BuildFileNameSuffix( this.Scope );

        /// <summary>   Gets or sets the full filename of the profile file. </summary>
        /// <value> The full filename of the profile file. </value>
        public string FullFileName { get; private set; }

        /// <summary>   Gets or sets the filename of the application data settings full file. </summary>
        /// <value> The filename of the application data settings full file. </value>
        private string AppDataSettingsFullFileName { get; set; }

        /// <summary>   The Application Context settings full file name. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <returns>   A string. </returns>
        private string AppContextSettingsFullFileName { get; set; }

        /// <summary>   Select file name. </summary>
        /// <remarks>   David, 2021-07-27. </remarks>
        /// <returns>   A string. </returns>
        private string SelectFileName()
        {
            return Scribe.IsAppContextRestricted ? this.AppDataSettingsFullFileName : this.AppContextSettingsFullFileName;
        }

        /// <summary>   Checks if the profile file exists. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <returns>   True if it exists; otherwise, false. </returns>
        public bool Exists()
        {
            return File.Exists( this.FullFileName );
        }

        /// <summary>
        /// Gets a value indicating whether this object is application context restricted.
        /// </summary>
        /// <value> True if this object is application context restricted, false if not. </value>
        public static bool IsAppContextRestricted => AppContext.BaseDirectory.StartsWith( Environment.GetFolderPath( Environment.SpecialFolder.ProgramFiles ), StringComparison.OrdinalIgnoreCase )
                    || AppContext.BaseDirectory.StartsWith( Environment.GetFolderPath( Environment.SpecialFolder.ProgramFilesX86 ), StringComparison.OrdinalIgnoreCase );

        #endregion

        #region " FILE NAME BUILDERS "

        /// <summary>   (Immutable) the application setting suffix. </summary>
        public const string AppSettingSuffix = ".Profile";

        /// <summary>   (Immutable) the user setting suffix. </summary>
        public const string UserSettingSuffix = ".UserProfile";

        /// <summary>   Builds the file name suffix. </summary>
        /// <remarks>   David, 2021-07-27. </remarks>
        /// <param name="scope">    The scope. </param>
        /// <returns>   A string. </returns>
        public static string BuildFileNameSuffix( ProfileScope scope )
        {
            return scope == ProfileScope.ApplicationScope ? Scribe.AppSettingSuffix : Scribe.UserSettingSuffix;
        }

        /// <summary>
        /// Builds application profile file title and extension consisting of the<para>
        /// <paramref name="assemblyName"/>+<see cref="FileNameSuffix"/>+.ini.</para>
        /// </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="assemblyName"> The assembly file title. </param>
        /// <param name="scope">        (Optional) The scope. </param>
        /// <returns>   A file title and extension. </returns>
        public static string BuildAppProfileFileName( string assemblyName, ProfileScope scope = ProfileScope.ApplicationScope )
        {
            return $"{assemblyName}{Scribe.BuildFileNameSuffix( scope )}{Scribe.DefaultExtension}";
        }

        /// <summary>
        /// Builds application profile file title and extension consisting of the<para>
        /// 'Assembly Name'+<see cref="FileNameSuffix"/>+.ini.</para>
        /// </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="assembly"> The assembly, which could be the Entry, Calling or Executing
        ///                         assembly. </param>
        /// <param name="scope">    (Optional) The scope. </param>
        /// <returns>   A file title and extension. </returns>
        public static string BuildAppProfileFileName( System.Reflection.Assembly assembly, ProfileScope scope = ProfileScope.ApplicationScope )
        {
            return Scribe.BuildAppProfileFileName( assembly.GetName().Name, scope );
        }

        /// <summary>
        /// Builds application profile full file name consisting of the<para>
        /// <paramref name="folderName"/>\'Assembly Name'+<see cref="FileNameSuffix"/>+.ini.</para>
        /// </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="folderName">   Pathname of the folder. </param>
        /// <param name="assembly">     The assembly, which could be the Entry, Calling or Executing
        ///                             assembly. </param>
        /// <param name="scope">        (Optional) The scope. </param>
        /// <returns>   A string. </returns>
        public static string BuildAppProfileFullFileName( string folderName, System.Reflection.Assembly assembly, ProfileScope scope = ProfileScope.ApplicationScope )
        {
            return System.IO.Path.Combine( folderName, Scribe.BuildAppProfileFileName( assembly, scope ) );
        }

        /// <summary>   Builds application context profile full file name. </summary>
        /// <remarks>   David, 2021-07-27. </remarks>
        /// <param name="assembly"> The assembly, which could be the Entry, Calling or Executing
        ///                         assembly. </param>
        /// <param name="scope">    (Optional) The scope. </param>
        /// <returns>   A string. </returns>
        public static string BuildAppContextProfileFullFileName( System.Reflection.Assembly assembly, ProfileScope scope = ProfileScope.ApplicationScope )
        {
            return System.IO.Path.Combine( AppContext.BaseDirectory, Scribe.BuildAppProfileFileName( assembly, scope ) );
        }

        /// <summary>   Builds assembly folder name. </summary>
        /// <remarks>   David, 2021-07-27. </remarks>
        /// <param name="assembly"> The assembly, which could be the Entry, Calling or Executing
        ///                         assembly. </param>
        /// <returns>   A string. </returns>
        public static string BuildAssemblyFolderName( System.Reflection.Assembly assembly )
        {
            var versionInfo = FileVersionInfo.GetVersionInfo( assembly.Location );
            return System.IO.Path.Combine( versionInfo.CompanyName, versionInfo.ProductName, versionInfo.ProductVersion );
        }

        /// <summary>   Builds application data profile full file name. </summary>
        /// <remarks>   David, 2021-07-27. </remarks>
        /// <param name="assembly"> The assembly, which could be the Entry, Calling or Executing
        ///                         assembly. </param>
        /// <param name="scope">    (Optional) The scope. </param>
        /// <returns>   A string. </returns>
        public static string BuildApplicationDataProfileFullFileName( System.Reflection.Assembly assembly, ProfileScope scope = ProfileScope.ApplicationScope )
        {
            return BuildAppProfileFullFileName( System.IO.Path.Combine( Environment.GetFolderPath( scope == ProfileScope.ApplicationScope
                                                                                                        ? Environment.SpecialFolder.CommonApplicationData
                                                                                                        : Environment.SpecialFolder.ApplicationData ),
                                                                         Scribe.BuildAssemblyFolderName( assembly ) ),
                                                  assembly, scope );
        }

        #endregion

    }

    /// <summary>   Values that represent profile scopes. </summary>
    /// <remarks>   David, 2021-07-27. </remarks>
    public enum ProfileScope
    {
        /// <summary>   An enum constant representing the application profile scope option. </summary>
        ApplicationScope,
        /// <summary>   An enum constant representing the user profile scope option. </summary>
        UserScope
    }

}
