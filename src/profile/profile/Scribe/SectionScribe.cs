using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace isr.IO.Profile
{

    /// <summary> Reads and writes private and public profile (INI Settings) sections. </summary>
    /// <remarks>
    /// Section and key names are not case-sensitive. Values are loaded into a hash table for fast access.
    /// Sections in the initialization file must have the following form:
    /// <code>
    ///     ; comment line
    ///     [section]
    ///     key=value
    /// </code>
    /// </remarks>
    public class SectionScribe : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SectionScribe" /> class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public SectionScribe( Scribe scribe, string sectionName )
        {
            this.Scribe = scribe;
            this.SectionName = sectionName;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Gets the dispose status sentinel of the base class.  This applies to the derived class
        /// provided proper implementation.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:ProfileScribe" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        protected virtual void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;

            try
            {
                if ( disposing )
                {
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/destructors
        /// </remarks>
        ~SectionScribe()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }

        #endregion

        #region " SCRIBE and SECTION "

        /// <summary>   Gets or sets the scribe. </summary>
        /// <value> The scribe. </value>
        public Scribe Scribe { get; private set; }

        /// <summary> Gets the name of the section. </summary>
        /// <remarks> Use This property to set or get the name of the INI Settings section. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value>
        /// <c>SectionName</c> is a String property that can be read from or written to (read or write)
        /// that specifies the name of the section in the INI Settings file.
        /// </value>
        public string SectionName { get; private set; }

        #endregion

        #region " LIST "

        /// <summary>   Gets a list of INI Settings from the <see cref="Scribe"/> cache. 
        /// The file must be first <see cref="Scribe.Read()"/></summary>
        /// <remarks>
        /// The format of the list of INI Settings is SettingName# e.g., <c>Setting1, Setting2</c> <para>
        /// The list is dimensioned starting with First Item, which defaults to 1.  For example, the
        /// first item value must be set to 0 in order to read INI Settings Setting0, Setting1,..'  or to
        /// -1 to read the list ordered as -1,0,1,...</para>
        /// This example writes a list of INI Settings to an INI file and then reads the list from the
        /// file.
        /// <code>
        /// Sub Form_Click
        ///   ' Declare some data storage
        ///   Dim i as Int32
        ///   ' Create an instance of the ISR INI Settings Scribe Class Dim ini as isr.IO.ProfileScribe
        ///   ' Create data to write to the INI Settings file Dim items As new List(of String)
        ///   For i = 0 to UBound(sngList)
        ///     items.Add(Log(convert.ToSingle(i+1))).ToString)
        ///   Next i
        ///   ' Set the INI Settings file name, section name, and setting name
        ///   Dim scribe As New Scribe()
        ///   Dim section As New Section( scribe, "List Section")
        ///   Dim listItemName = "List Item"
        ///   ' Write the list to the file.
        ///   section.WriteList(listItemName, items)
        ///   ' Read the list from the file  
        ///   items = section.ReadList(listItemName)
        /// End Sub </code>
        /// </remarks>
        /// <param name="listItemName">     Name of the list item. </param>
        /// <param name="firstItemNumber">  (Optional) is an Int32 expression that specifies the number
        ///                                 of the first list item (default is 1) </param>
        /// <returns>   The list. </returns>
        public IEnumerable<string> GetList( string listItemName, int firstItemNumber = 1 )
        {
            var items = new List<string>();
            int currentItemNumber = firstItemNumber;
            string itemValue;

            // Loop through the list data.
            do
            {

                // Read data from the INI Settings file
                itemValue = this.Scribe.GetValue( this.SectionName, SectionScribe.BuildListItemName( listItemName, currentItemNumber ), string.Empty );
                if ( itemValue.Length > 0 )
                {
                    // add item to the list
                    items.Add( itemValue );

                    // Increment item count
                    currentItemNumber += 1;
                }
            }
            while ( itemValue.Length != 0 );

            // return the list
            return items;
        }

        /// <summary>
        /// Sets a list of INI Settings onto the <see cref="Scribe"/> cache. A
        /// <see cref="Scribe.Write()"/> must be done to save the values.
        /// </summary>
        /// <remarks>
        /// The format of the list of INI Settings is SettingName# e.g., <c>Setting1, Setting2</c> <para>
        /// The list is dimensioned starting with First Item, which defaults to 1.  For example, the
        /// first item value must be set to 0 in order to read INI Settings Setting0, Setting1,..'  or to
        /// -1 to read the list ordered as -1,0,1,...</para>
        /// This example writes a list of INI Settings to an INI file and then reads the list from the
        /// file.
        /// </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="listItemName">     Name of the list item. </param>
        /// <param name="values">           The list values. </param>
        /// <param name="firstItemNumber">  (Optional) is an Int32 expression that specifies the number
        ///                                 of the first list item (default is 1) </param>
        public void SetList( string listItemName, IEnumerable<string> values, int firstItemNumber = 1 )
        {
            if ( values is null )
                throw new ArgumentNullException( nameof( values ) );
            int currentItermNumber = firstItemNumber;
            // Loop through the list data.
            foreach ( var itemValue in values )
            {
                // Write the list setting
                this.Scribe.SetValue( this.SectionName, SectionScribe.BuildListItemName( listItemName, currentItermNumber ), itemValue );
                currentItermNumber += 1;
            }
        }
        
        /// <summary>   Builds list item name. </summary>
        /// <remarks>
        /// Use This property to get the name of the list setting for reading or write a list item.
        /// </remarks>
        /// <param name="listItemName"> Name of the list item. </param>
        /// <param name="itemNumber">   Is an Int32 expression that specifies the item number in the
        ///                             list. </param>
        /// <returns>   <c>_listSettingName</c> is a read only string property. </returns>
        private static string BuildListItemName( string listItemName,  int itemNumber )
        {
            return $"{listItemName}{itemNumber}";
        }

        #endregion

        #region " READ LIST ITEM "

        /// <summary>   Get a "T:Boolean" list value from the <see cref="Scribe"/> cache. 
        /// The cache must be <see cref="Scribe.Read()"/> first.</summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="listItemName"> Name of the list item. </param>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="defaultValue"> A <see cref="System.Boolean">String</see> expression that
        ///                             specifies a default value to return if the key is not found. </param>
        /// <returns>   A <see cref="System.Boolean">Boolean</see> data type. </returns>
        public bool GetListItem( string listItemName, int itemNumber, bool defaultValue )
        {
            return this.Scribe.GetValue( this.SectionName, SectionScribe.BuildListItemName( listItemName, itemNumber ), defaultValue );
        }

        /// <summary>
        /// Gets a "T:Byte" list value from the <see cref="Scribe"/> cache. The cache must be
        /// <see cref="Scribe.Read()"/> first.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="listItemName"> Name of the list item. </param>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="defaultValue"> A <see cref="System.Byte">String</see> expression that specifies
        ///                             a default value to return if the key is not found. </param>
        /// <returns>   A <see cref="System.Byte">Byte</see> data type. </returns>
        public byte GetListItem( string listItemName, int itemNumber, byte defaultValue )
        {
            return this.Scribe.GetValue( this.SectionName, SectionScribe.BuildListItemName( listItemName, itemNumber ), defaultValue );
        }

        /// <summary>
        /// Gets a "T:DateTime" list value the <see cref="Scribe"/> cache. The cache must be
        /// <see cref="Scribe.Read()"/> first.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="listItemName"> Name of the list item. </param>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="defaultValue"> A <see cref="System.DateTime">String</see> expression that
        ///                             specifies a default value to return if the key is not found. </param>
        /// <returns>   A <see cref="System.DateTime">DateTime</see> data type. </returns>
        public DateTime GetListItem( string listItemName, int itemNumber, DateTime defaultValue )
        {
            return this.Scribe.GetValue( this.SectionName, SectionScribe.BuildListItemName( listItemName, itemNumber ), defaultValue );
        }

        /// <summary>
        /// Gets a "T:DateTimeOffset" list value from the <see cref="Scribe"/> cache. The cache must be
        /// <see cref="Scribe.Read()"/> first.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="listItemName"> Name of the list item. </param>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="defaultValue"> A <see cref="System.DateTime">String</see> expression that
        ///                             specifies a default value to return if the key is not found. </param>
        /// <returns>   A <see cref="System.DateTimeOffset">DateTimeOffset</see> data type. </returns>
        public DateTimeOffset GetListItem( string listItemName, int itemNumber, DateTimeOffset defaultValue )
        {
            return this.Scribe.GetValue( this.SectionName, SectionScribe.BuildListItemName( listItemName, itemNumber ), defaultValue );
        }

        /// <summary>
        /// Gets a "T:Decimal" list value from the <see cref="Scribe"/> cache. The cache must be
        /// <see cref="Scribe.Read()"/> first.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="listItemName"> Name of the list item. </param>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="defaultValue"> A <see cref="System.Decimal">String</see> expression that
        ///                             specifies a default value to return if the key is not found. </param>
        /// <returns>   A <see cref="System.Decimal">Decimal</see> data type. </returns>
        public decimal GetListItem( string listItemName, int itemNumber, decimal defaultValue )
        {
            return this.Scribe.GetValue( this.SectionName, SectionScribe.BuildListItemName( listItemName, itemNumber ), defaultValue );
        }

        /// <summary>
        /// Gets a "T:Double" list value from the <see cref="Scribe"/> cache. The cache must be
        /// <see cref="Scribe.Read()"/> first.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="listItemName"> Name of the list item. </param>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="defaultValue"> A <see cref="System.Double">String</see> expression that
        ///                             specifies a default value to return if the key is not found. </param>
        /// <returns>   A <see cref="System.Double">Double</see> data type. </returns>
        public double GetListItem( string listItemName, int itemNumber, double defaultValue )
        {
            return this.Scribe.GetValue( this.SectionName, SectionScribe.BuildListItemName( listItemName, itemNumber ), defaultValue );
        }

        /// <summary>
        /// Gets a "T:Int16" list value from the <see cref="Scribe"/> cache. The cache must be
        /// <see cref="Scribe.Read()"/> first.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="listItemName"> Name of the list item. </param>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="defaultValue"> A <see cref="System.Int16">String</see> expression that specifies
        ///                             a default value to return if the key is not found. </param>
        /// <returns>   A <see cref="System.Int16">Int16</see> data type. </returns>
        public Int16 GetListItem( string listItemName, int itemNumber, Int16 defaultValue )
        {
            return this.Scribe.GetValue( this.SectionName, SectionScribe.BuildListItemName( listItemName, itemNumber ), defaultValue );
        }

        /// <summary>
        /// Gets a "T:Int32" list value from the <see cref="Scribe"/> cache. The cache must be
        /// <see cref="Scribe.Read()"/> first.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="listItemName"> Name of the list item. </param>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="defaultValue"> A <see cref="System.Int32">String</see> expression that specifies
        ///                             a default value to return if the key is not found. </param>
        /// <returns>   A <see cref="System.Int32">Int32</see> data type. </returns>
        public Int32 GetListItem( string listItemName, int itemNumber, Int32 defaultValue )
        {
            return this.Scribe.GetValue( this.SectionName, SectionScribe.BuildListItemName( listItemName, itemNumber ), defaultValue );
        }

        /// <summary>
        /// Gets a "T:Int64" list value from the <see cref="Scribe"/> cache. The cache must be
        /// <see cref="Scribe.Read()"/> first.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="listItemName"> Name of the list item. </param>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="defaultValue"> A <see cref="System.Int64">String</see> expression that specifies
        ///                             a default value to return if the key is not found. </param>
        /// <returns>   A <see cref="System.Int64">Int64</see> data type. </returns>
        public Int64 GetListItem( string listItemName, int itemNumber, Int64 defaultValue )
        {
            return this.Scribe.GetValue( this.SectionName, SectionScribe.BuildListItemName( listItemName, itemNumber ), defaultValue );
        }

        /// <summary>
        /// Gets a "T:Single" list value from the <see cref="Scribe"/> cache. The cache must be
        /// <see cref="Scribe.Read()"/> first.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="listItemName"> Name of the list item. </param>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="defaultValue"> A <see cref="System.Single">String</see> expression that
        ///                             specifies a default value to return if the key is not found. </param>
        /// <returns>   A <see cref="System.Single">Single</see> data type. </returns>
        public float GetListItem( string listItemName, int itemNumber, float defaultValue )
        {
            return this.Scribe.GetValue( this.SectionName, SectionScribe.BuildListItemName( listItemName, itemNumber ), defaultValue );
        }

        /// <summary>
        /// Gets a "T:String" list value from the <see cref="Scribe"/> cache. The cache must be
        /// <see cref="Scribe.Read()"/> first.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="listItemName"> Name of the list item. </param>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="defaultValue"> A <see cref="System.String">String</see> expression that
        ///                             specifies a default value to return if the key is not found. </param>
        /// <returns>   A <see cref="System.String">String</see> data type. </returns>
        public string GetListItem( string listItemName, int itemNumber, string defaultValue )
        {
            return this.Scribe.GetValue( this.SectionName, SectionScribe.BuildListItemName( listItemName, itemNumber ), defaultValue );
        }

        #endregion

        #region " READ SECTION VALUE "

        /// <summary>
        /// Gets a "T:Boolean" Section value from the <see cref="Scribe"/> cache. The cache must be
        /// <see cref="Scribe.Read()"/> first.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="defaultValue"> A <see cref="System.Boolean">String</see> expression that
        ///                             specifies a default value to return if the key is not found. </param>
        /// <returns>   A <see cref="System.Boolean">Boolean</see> data type. </returns>
        public bool GetValue( string settingsName, bool defaultValue )
        {
            return this.Scribe.GetValue( this.SectionName, settingsName, defaultValue );
        }

        /// <summary>
        /// Gets a "T:Byte" Section value from the <see cref="Scribe"/> cache. The cache must be
        /// <see cref="Scribe.Read()"/> first.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="defaultValue"> A <see cref="System.Byte">String</see> expression that specifies
        ///                             a default value to return if the key is not found. </param>
        /// <returns>   A <see cref="System.Byte">Byte</see> data type. </returns>
        public byte GetValue( string settingsName, byte defaultValue )
        {
            return this.Scribe.GetValue( this.SectionName, settingsName, defaultValue );
        }

        /// <summary>
        /// Gets a "T:DateTime" Section value from the <see cref="Scribe"/> cache. The cache must be
        /// <see cref="Scribe.Read()"/> first.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="defaultValue"> A <see cref="System.DateTime">String</see> expression that
        ///                             specifies a default value to return if the key is not found. </param>
        /// <returns>   A <see cref="System.DateTime">DateTime</see> data type. </returns>
        public DateTime GetValue( string settingsName, DateTime defaultValue )
        {
            return this.Scribe.GetValue( this.SectionName, settingsName, defaultValue );
        }

        /// <summary>
        /// Gets a string value from the <see cref="Scribe"/> cache. The cache must be
        /// <see cref="Scribe.Read()"/> first.
        /// </summary>
        /// <remarks>   Use This method to read a String value from an INI file. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="defaultValue"> A <see cref="System.DateTimeOffset">String</see> expression that
        ///                             specifies a default value to return if the key is not found. </param>
        /// <returns>   A <see cref="System.DateTimeOffset">DateTimeOffset</see> data type. </returns>
        public DateTimeOffset GetValue( string settingsName, DateTimeOffset defaultValue )
        {
            return this.Scribe.GetValue( this.SectionName, settingsName, defaultValue );
        }

        /// <summary>
        /// Gets a "T:Decimal" Section value from the <see cref="Scribe"/> cache. The cache must be
        /// <see cref="Scribe.Read()"/> first.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="defaultValue"> A <see cref="System.Decimal">String</see> expression that
        ///                             specifies a default value to return if the key is not found. </param>
        /// <returns>   A <see cref="System.Decimal">Decimal</see> data type. </returns>
        public decimal GetValue( string settingsName, decimal defaultValue )
        {
            return this.Scribe.GetValue( this.SectionName, settingsName, defaultValue );
        }

        /// <summary>
        /// Gets a "T:Double" Section value from the <see cref="Scribe"/> cache. The cache must be
        /// <see cref="Scribe.Read()"/> first.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="defaultValue"> A <see cref="System.Double">String</see> expression that
        ///                             specifies a default value to return if the key is not found. </param>
        /// <returns>   A <see cref="System.Double">Double</see> data type. </returns>
        public double GetValue( string settingsName, double defaultValue )
        {
            return this.Scribe.GetValue( this.SectionName, settingsName, defaultValue );
        }

        /// <summary>
        /// Gets a "T:Int16" Section value from the <see cref="Scribe"/> cache. The cache must be
        /// <see cref="Scribe.Read()"/> first.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="defaultValue"> A <see cref="System.Int16">String</see> expression that specifies
        ///                             a default value to return if the key is not found. </param>
        /// <returns>   A <see cref="System.Int16">Int16</see> data type. </returns>
        public short GetValue( string settingsName, short defaultValue )
        {
            return this.Scribe.GetValue( this.SectionName, settingsName, defaultValue );
        }

        /// <summary>
        /// Gets a "T:Int32" Section value from the <see cref="Scribe"/> cache. The cache must be
        /// <see cref="Scribe.Read()"/> first.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="defaultValue"> A <see cref="System.Int32">String</see> expression that specifies
        ///                             a default value to return if the key is not found. </param>
        /// <returns>   A <see cref="System.Int32">Int32</see> data type. </returns>
        public int GetValue( string settingsName, int defaultValue )
        {
            return this.Scribe.GetValue( this.SectionName, settingsName, defaultValue );
        }

        /// <summary>
        /// Gets a "T:Int64" Section value from the <see cref="Scribe"/> cache. The cache must be
        /// <see cref="Scribe.Read()"/> first.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="defaultValue"> A <see cref="System.Int64">String</see> expression that specifies
        ///                             a default value to return if the key is not found. </param>
        /// <returns>   A <see cref="System.Int64">Int64</see> data type. </returns>
        public long GetValue( string settingsName, long defaultValue )
        {
            return this.Scribe.GetValue( this.SectionName, settingsName, defaultValue );
        }

        /// <summary>
        /// Gets a "T:Single" Section value from the <see cref="Scribe"/> cache. The cache must be
        /// <see cref="Scribe.Read()"/> first.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="defaultValue"> A <see cref="System.Single">String</see> expression that
        ///                             specifies a default value to return if the key is not found. </param>
        /// <returns>   A <see cref="System.Single">Single</see> data type. </returns>
        public float GetValue( string settingsName, float defaultValue )
        {
            return this.Scribe.GetValue( this.SectionName, settingsName, defaultValue );
        }

        /// <summary>
        /// Gets a "T:String" Section value from the <see cref="Scribe"/> cache. The cache must be
        /// <see cref="Scribe.Read()"/> first.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="defaultValue"> A <see cref="System.String">String</see> expression that
        ///                             specifies a default value to return if the key is not found. </param>
        /// <returns>   A <see cref="System.String">String</see> data type. </returns>
        public string GetValue( string settingsName, string defaultValue )
        {
            return this.Scribe.GetValue( this.SectionName, settingsName, defaultValue );
        }

        #endregion

        #region " WRITE LIST ITEM "

        /// <summary>
        /// Sets a "T:Boolean" value onto the <see cref="Scribe"/> cache. A <see cref="Scribe.Write()"/>
        /// must be done to save the values.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="listItemName"> Name of the list item. </param>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="value">        A <see cref="System.Boolean">Boolean</see> expression that
        ///                             specifies the value to write. </param>
        public void SetValue( string listItemName, int itemNumber, bool value )
        {
            this.Scribe.SetValue( this.SectionName, SectionScribe.BuildListItemName( listItemName, itemNumber ), value.ToString() );
        }

        /// <summary>
        /// Sets a "T:DateTime" value onto the <see cref="Scribe"/> cache. A <see cref="Scribe.Write()"/>
        /// must be done to save the values.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="listItemName"> Name of the list item. </param>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="value">        A <see cref="System.DateTime">DateTime</see> expression that
        ///                             specifies the value to write. </param>
        public void SetValue( string listItemName, int itemNumber, DateTime value )
        {
            this.Scribe.SetValue( this.SectionName, SectionScribe.BuildListItemName( listItemName, itemNumber ), value.ToString() );
        }

        /// <summary>
        /// Sets a "T:DateTime" value onto the <see cref="Scribe"/> cache. A <see cref="Scribe.Write()"/>
        /// must be done to save the values.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="listItemName"> Name of the list item. </param>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="value">        A <see cref="System.DateTimeOffset">DateTimeOffset</see>
        ///                             expression that specifies the value to write. </param>
        public void SetValue( string listItemName, int itemNumber, DateTimeOffset value )
        {
            this.Scribe.SetValue( this.SectionName, SectionScribe.BuildListItemName( listItemName, itemNumber ), value.ToString() );
        }

        /// <summary>
        /// Sets a "T:Decimal" value onto the <see cref="Scribe"/> cache. A <see cref="Scribe.Write()"/>
        /// must be done to save the values.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="listItemName"> Name of the list item. </param>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="value">        A <see cref="System.Decimal">Decimal</see> expression that
        ///                             specifies the value to write. </param>
        public void SetValue( string listItemName, int itemNumber, decimal value )
        {
            this.Scribe.SetValue( this.SectionName, SectionScribe.BuildListItemName( listItemName, itemNumber ), value.ToString() );
        }

        /// <summary>
        /// Sets a "T:Double" value onto the <see cref="Scribe"/> cache. A <see cref="Scribe.Write()"/>
        /// must be done to save the values.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="listItemName"> Name of the list item. </param>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="value">        A <see cref="System.Double">Double</see> expression that
        ///                             specifies the value to write. </param>
        public void SetValue( string listItemName, int itemNumber, double value )
        {
            this.Scribe.SetValue( this.SectionName, SectionScribe.BuildListItemName( listItemName, itemNumber ), value.ToString() );
        }

        /// <summary>
        /// Sets a "T:Int64" value onto the <see cref="Scribe"/> cache. A <see cref="Scribe.Write()"/>
        /// must be done to save the values.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="listItemName"> Name of the list item. </param>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="value">        A <see cref="System.Int64">Int64</see> expression that specifies
        ///                             the value to write. </param>
        public void SetValue( string listItemName, int itemNumber, long value )
        {
            this.Scribe.SetValue( this.SectionName, SectionScribe.BuildListItemName( listItemName, itemNumber ), value.ToString() );
        }

        /// <summary>
        /// Sets a "T:String" value onto the <see cref="Scribe"/> cache. A <see cref="Scribe.Write()"/>
        /// must be done to save the values.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="listItemName"> Name of the list item. </param>
        /// <param name="itemNumber">   Specifies the list item number to read. </param>
        /// <param name="value">        A <see cref="System.String">String</see> expression that
        ///                             specifies the value to write. </param>
        public void SetValue( string listItemName, int itemNumber, string value )
        {
            this.Scribe.SetValue( this.SectionName, SectionScribe.BuildListItemName( listItemName, itemNumber ), value );
        }

        #endregion

        #region " WRITE SECTION VALUE "

        /// <summary> Sets a "T:Boolean" value onto the <see cref="Scribe"/> cache.
        /// A <see cref="Scribe.Write()"/> must be done to save the values. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="value">        A <see cref="System.Boolean">Boolean</see> expression that
        /// specifies the value to write. </param>
        public void SetValue( string settingsName, bool value )
        {
            this.Scribe.SetValue( this.SectionName, settingsName, value.ToString() );
        }

        /// <summary> Sets a "T:DateTime" value onto the <see cref="Scribe"/> cache.
        /// A <see cref="Scribe.Write()"/> must be done to save the values. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="value">        A <see cref="System.DateTime">DateTime</see> expression that
        /// specifies the value to write. </param>
        public void SetValue( string settingsName, DateTime value )
        {
            this.Scribe.SetValue( this.SectionName, settingsName, value.ToString() );
        }

        /// <summary>
        /// Sets a "T:DateTimeOffset" value onto the <see cref="Scribe"/> cache. A
        /// <see cref="Scribe.Write()"/> must be done to save the values.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="value">        A <see cref="System.DateTimeOffset">DateTimeOffset</see>
        ///                             expression that specifies the value to write. </param>
        public void SetValue( string settingsName, DateTimeOffset value )
        {
            this.Scribe.SetValue( this.SectionName, settingsName, value.ToString() );
        }

        /// <summary>
        /// Sets a "T:Decimal" value onto the <see cref="Scribe"/> cache. A <see cref="Scribe.Write()"/>
        /// must be done to save the values.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="value">        A <see cref="System.Decimal">Decimal</see> expression that
        ///                             specifies the value to write. </param>
        public void SetValue( string settingsName, decimal value )
        {
            this.Scribe.SetValue( this.SectionName, settingsName, value.ToString() );
        }

        /// <summary>
        /// Sets a "T:Double" value onto the <see cref="Scribe"/> cache. A <see cref="Scribe.Write()"/>
        /// must be done to save the values.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="value">        A <see cref="System.Double">Double</see> expression that
        ///                             specifies the value to write. </param>
        public void SetValue( string settingsName, double value )
        {
            this.Scribe.SetValue( this.SectionName, settingsName, value.ToString() );
        }

        /// <summary>
        /// Sets a "T:Int64" value onto the <see cref="Scribe"/> cache. A <see cref="Scribe.Write()"/>
        /// must be done to save the values.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="value">        A <see cref="System.Int64">Int64</see> expression that specifies
        ///                             the value to write. </param>
        public void SetValue( string settingsName, long value )
        {
            this.Scribe.SetValue( this.SectionName, settingsName, value.ToString() );
        }

        /// <summary>
        /// Sets a "T:String" value onto the <see cref="Scribe"/> cache. A <see cref="Scribe.Write()"/>
        /// must be done to save the values.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="settingsName"> Name of the settings. </param>
        /// <param name="value">        A <see cref="System.String">String</see> expression that
        ///                             specifies the value to write. </param>
        public void SetValue( string settingsName, string value )
        {
            this.Scribe.SetValue( this.SectionName, settingsName, value );
        }

        #endregion

    }
}
