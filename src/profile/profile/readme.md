# About

isr.IO.Profile is a .Net library for reading INI files.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

isr.IO.Profile is released as open source under the MIT license.
Bug reports and contributions are welcome at the [IO Repository].

[IO Repository]: https://bitbucket.org/davidhary/dn.io

