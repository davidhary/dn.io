using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyDescription( "Structured Read and Writer Unit Tests" )]
[assembly: ComVisible( false )]
[assembly: CLSCompliant( true )]
