using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.IO.StructuredScribe.MSTest
{

    /// <summary>
    /// Contains all unit tests for the <see cref="isr.IO.StructuredReader">Reader</see> class.
    /// </summary>
    /// <remarks>
    /// (c) 2007 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2007-12-08, 6.0.2898. Created </para>
    /// </remarks>
    [TestClass()]
    public class ReaderTest
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                // write a record of data for testing to the test file.
                _Writer = new isr.IO.StructuredWriter() {
                    FullFileName = System.IO.Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.CommonDocuments ), $"{testContext.FullyQualifiedTestClassName}.csv" )
                };
                // ask for a new file.
                _Writer.NewFile();

                // ask for a new record
                _Writer.NewRow();

                // add data to the record
                int index = 0; ReaderTest._BooleanFieldIndex = index;
                _ = _Writer.AddField( _BooleanValue );
                index += 1; ReaderTest._ByteFieldIndex = index;
                _ = _Writer.AddField( _ByteValue );
                index += 1; ReaderTest._DateFieldIndex = index;
                _ = _Writer.AddField( DateValue );
                index += 1; ReaderTest._OleDateTimeFieldIndex = index;
                _ = _Writer.AddField( DateValue.ToOADate() );
                index += 1; ReaderTest._DecimalFieldIndex = index;
                _ = _Writer.AddField( _DecimalValue );
                index += 1; ReaderTest._DoubleFieldIndex = index;
                _ = _Writer.AddField( _DoubleValue );
                index += 1; ReaderTest._Int16FieldIndex = index;
                _ = _Writer.AddField( _Int16Value );
                index += 1; ReaderTest._Int32FieldIndex = index;
                _ = _Writer.AddField( _IntegerValue );
                index += 1; ReaderTest._Int64FieldIndex = index;
                _ = _Writer.AddField( _LongValue );
                index += 1; ReaderTest._FloatFieldIndex = index;
                _ = _Writer.AddField( _FloatValue );
                index += 1; ReaderTest._StringFieldIndex = index;
                _ = _Writer.AddField( _StringValue );
                _Writer.WriteFields();
                _Reader = new StructuredReader( _Writer.FullFileName );
                Assert.IsTrue( _Reader.ReadFields(), "Structured.Reader.ReadFields from: {0} ", _Reader.FullFileName );
            }
            catch ( Exception )
            {
                // close to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }
            }
        }

        /// <summary> Runs code after all tests in a class have run. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            try
            {
                if ( _Reader is object )
                {
                    _Reader.Dispose();
                    _Reader = null;
                }

                if ( _Writer is object )
                {
                    _Writer = null;
                }
            }
            catch
            {
            }
            finally
            {
                _Reader = null;
                _Writer = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " ADDITIONAL TEST ATTRIBUTED METHODS "

        /// <summary> Name of the section. </summary>
        private const string _SectionName = "Unit Tests";

        /// <summary> The reader. </summary>
        private static StructuredReader _Reader;

        /// <summary> The writer. </summary>
        private static StructuredWriter _Writer;

        /// <summary> The date value Date/Time. </summary>
        private static readonly DateTime DateValue = DateTime.FromOADate( DateTime.Now.ToOADate() );

        /// <summary> The string value. </summary>
        private const string _StringValue = "String value";

        /// <summary> True to boolean value. </summary>
        private const bool _BooleanValue = true;

        /// <summary> The byte value. </summary>
        private const byte _ByteValue = 129;

        /// <summary> The decimal value. </summary>
        private const decimal _DecimalValue = 12.12m;

        /// <summary> The double value. </summary>
        private const double _DoubleValue = Math.PI;

        /// <summary> The short value. </summary>
        private const short _Int16Value = 16;

        /// <summary> The integer value. </summary>
        private const int _IntegerValue = 32;

        /// <summary> The long value. </summary>
        private const long _LongValue = 64L;

        /// <summary> The single value. </summary>
        private const float _FloatValue = float.MinValue;

        #endregion

        private static int _BooleanFieldIndex;

        /// <summary>   (Unit Test Method) field boolean should read and parse.
        [TestMethod()]
        public void FieldBooleanShouldReadAndParse()
        {
            bool actual = _Reader.ParseBoolField( _BooleanFieldIndex );
            bool expected = _BooleanValue;
            Assert.AreEqual( expected, actual, $"{nameof(StructuredReader.ParseBoolField)} should parse" );
            Assert.AreEqual( expected, _Reader.ParseField<bool>( _BooleanFieldIndex ), $"{nameof( StructuredReader.ParseField)} should parse" );
        }

        private static int _ByteFieldIndex;

        /// <summary>   (Unit Test Method) field byte should read and parse. </summary>
        [TestMethod()]
        public void FieldByteShouldReadAndParse()
        {
            byte actual = _Reader.ParseByteField( _ByteFieldIndex );
            byte expected = _ByteValue;
            Assert.AreEqual( expected, actual, $"{nameof( StructuredReader.ParseByteField )} should parse" );
            Assert.AreEqual( expected, _Reader.ParseField<byte>( _ByteFieldIndex ), $"{nameof( StructuredReader.ParseField )} should parse" );
        }

        private static int _DateFieldIndex;

        /// <summary>   (Unit Test Method) field date should read and parse. </summary>
        [TestMethod()]
        public void FieldDateShouldReadAndParse()
        {
            DateTime actual = _Reader.ParseDateTimeField( _DateFieldIndex );
            DateTime expected = DateTime.FromOADate( DateValue.ToOADate() );
            // accurate withing 1 second.
            Assert.AreEqual( expected.ToLongDateString(), actual.ToLongDateString(), $"{nameof( StructuredReader.ParseDateTimeField )} should parse" );
            Assert.AreEqual( expected.ToLongTimeString(), actual.ToLongTimeString(), $"{nameof( StructuredReader.ParseDateTimeField )} should parse" );
            Assert.AreEqual( expected.ToLongTimeString(), _Reader.ParseField<DateTime>( _DateFieldIndex ).ToLongTimeString(), $"{nameof( StructuredReader.ParseField )} should parse" );
        }

        private static int _OleDateTimeFieldIndex;

        /// <summary>   (Unit Test Method) field date time should read and parse. </summary>
        [TestMethod()]
        public void FieldDateTimeShouldReadAndParse()
        {
            double actual = _Reader.ParseDoubleField( _OleDateTimeFieldIndex );
            var expected = DateValue;
            Assert.AreEqual( expected, DateTime.FromOADate( actual ), $"{nameof(DateTime.FromOADate)}({nameof( StructuredReader.ParseDoubleField )}) should parse" );
            Assert.AreEqual( expected, DateTime.FromOADate( _Reader.ParseField<double>( _OleDateTimeFieldIndex ) ), $"{nameof( StructuredReader.ParseField )} should parse" );
        }

        private static int _DecimalFieldIndex;

        /// <summary>   (Unit Test Method) field decimal should read and parse. </summary>
        /// <remarks>   David, 2021-08-24. </remarks>
        [TestMethod()]
        public void FieldDecimalShouldReadAndParse()
        {
            decimal actual = _Reader.ParseDecimalField( _DecimalFieldIndex );
            decimal expected = _DecimalValue;
            Assert.AreEqual( expected, actual, $"{nameof( StructuredReader.ParseDecimalField )} should parse" );
            Assert.AreEqual( expected, _Reader.ParseField<decimal>( _DecimalFieldIndex ), $"{nameof( StructuredReader.ParseField )} should parse" );
        }

        private static int _DoubleFieldIndex;

        /// <summary>   (Unit Test Method) field double should read and parse. </summary>
        [TestMethod()]
        public void FieldDoubleShouldReadAndParse()
        {
            double actual = _Reader.ParseDoubleField( _DoubleFieldIndex );
            double expected = _DoubleValue;
            Assert.AreEqual( expected, actual, 0.000000000001d, $"{nameof( StructuredReader.ParseDoubleField )} should parse" );
            Assert.AreEqual( expected, _Reader.ParseField<double>( _DoubleFieldIndex ), 0.000000000001d, $"{nameof( StructuredReader.ParseField )} should parse" );
        }

        private static int _Int16FieldIndex;

        /// <summary>   (Unit Test Method) field int 16 should read and parse. </summary>
        [TestMethod()]
        public void FieldInt16ShouldReadAndParse()
        {
            short actual = _Reader.ParseInt16Field( _Int16FieldIndex );
            short expected = _Int16Value;
            Assert.AreEqual( expected, actual, $"{nameof( StructuredReader.ParseInt16Field )} should parse" );
            Assert.AreEqual( expected, _Reader.ParseField<Int16>( _Int16FieldIndex ), $"{nameof( StructuredReader.ParseField )} should parse" );
        }

        private static int _Int32FieldIndex;

        /// <summary>   (Unit Test Method) field int 32 should read and parse. </summary>
        [TestMethod()]
        public void FieldInt32ShouldReadAndParse()
        {
            int actual = _Reader.ParseInt32Field( _Int32FieldIndex );
            int expected = _IntegerValue;
            Assert.AreEqual( expected, actual, $"{nameof( StructuredReader.ParseInt32Field )} should parse" );
            Assert.AreEqual( expected, _Reader.ParseField<Int32>( _Int32FieldIndex ), $"{nameof( StructuredReader.ParseField )} should parse" );
        }

        private static int _Int64FieldIndex;

        /// <summary>   (Unit Test Method) field int 64 should read and parse. </summary>
        [TestMethod()]
        public void FieldInt64ShouldReadAndParse()
        {
            long actual = _Reader.ParseInt64Field( _Int64FieldIndex );
            long expected = _LongValue;
            Assert.AreEqual( expected, actual, $"{nameof( StructuredReader.ParseInt64Field )} should parse" );
            Assert.AreEqual( expected, _Reader.ParseField<Int64>( _Int64FieldIndex ), $"{nameof( StructuredReader.ParseField )} should parse" );
        }

        private static int _FloatFieldIndex;

        /// <summary>   (Unit Test Method) field float should read and parse. </summary>
        [TestMethod()]
        public void FieldFloatShouldReadAndParse()
        {
            float actual = _Reader.ParseFloatField( _FloatFieldIndex );
            float expected = _FloatValue;
            Assert.AreEqual( Math.Abs( actual / expected ), 1d, 0.000001d, $"{nameof( StructuredReader.ParseFloatField )} '{expected}' should equal '{actual }'" );
            actual = _Reader.ParseField<float>( _FloatFieldIndex );
            Assert.AreEqual( Math.Abs( actual / expected ), 1d, 0.000001d, $"{nameof( StructuredReader.ParseField )} '{expected}' should equal '{actual }'" );
        }

        private static int _StringFieldIndex;

        /// <summary>   (Unit Test Method) field string should read. </summary>
        /// <remarks>   David, 2021-08-24. </remarks>
        [TestMethod()]
        public void FieldStringShouldRead()
        {
            string actual = _Reader.SelectField( _StringFieldIndex );
            string expected = _StringValue;
            Assert.AreEqual( expected, actual, $"{nameof( StructuredReader.SelectField )} should select" );
        }

        /// <summary>   (Unit Test Method) fields should read. </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        [TestMethod()]
        public void FieldsShouldRead()
        {
            if ( !_Reader.Parser.EndOfData )
            {
                Assert.IsTrue( _Reader.ReadFields(), $"{nameof( StructuredReader.ReadFields)} should read" );
            }
        }
    }
}
