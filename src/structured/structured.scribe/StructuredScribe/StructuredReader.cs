using System;
using System.Collections.Generic;
using System.Diagnostics;

using isr.IO.Structured.Scribe;

namespace isr.IO
{

    /// <summary> Reads delimited files. </summary>
    /// <remarks>
    /// (c) 2006 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2006-04-20, 1.1.2301.x. </para>
    /// </remarks>
    public class StructuredReader : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>

        // instantiate the base class
        public StructuredReader() : this( DefaultFullFileName )
        {
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-08-24. </remarks>
        /// <param name="fullFileName"> Specifies the file path name. </param>
        /// <param name="delimiter">    (Optional) The delimiter. </param>
        public StructuredReader( string fullFileName, char delimiter = ',' ) : base()
        {
            this._FullFileName = fullFileName;

            // open the parser
            this.Parser = new TextFieldParser( fullFileName, FieldType.Delimited, delimiter );
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Gets the dispose status sentinel of the base class.  This applies to the derived class
        /// provided proper implementation.
        /// </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        protected virtual void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                    this.Parser?.Dispose();
                    this.Parser = null;
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " SHARED "

        /// <summary> Gets the default extension. </summary>
        public const string DefaultExtension = ".csv";

        /// <summary>
        /// Returns the default file name for the structured I/O.  This would be the executable file name
        /// appended with the
        /// <see cref="DefaultExtension">default extension</see>.
        /// </summary>
        /// <value> The default file path name. </value>
        public static string DefaultFullFileName => System.IO.Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.CommonDocuments ),
                                                                            System.Reflection.Assembly.GetExecutingAssembly().GetName().Name,
                                                                            DefaultExtension );

        /// <summary>
        /// Return file rows as a collection of rows allowing each row to be of different length.
        /// </summary>
        /// <remarks>   David, 2021-08-24. </remarks>
        /// <param name="fullFileName"> Specifies the file path name. </param>
        /// <param name="delimiter">    The delimiter. </param>
        /// <returns>   The array of arrays of rows. </returns>
        public static System.Collections.ObjectModel.ReadOnlyCollection<string[]> ReadRows( string fullFileName, char delimiter )
        {
            var list = new List<string[]>();
            using ( var MyReader = new TextFieldParser( fullFileName, FieldType.Delimited, delimiter ) )
            {
                string[] currentRow;
                // Loop through all of the fields in the file. 
                // If any rows are corrupt, report an error and continue parsing. 
                while ( !MyReader.EndOfData )
                {
                    currentRow = MyReader.ReadFields();
                    if ( currentRow is object && currentRow.Length > 0 )
                    {
                        list.Add( currentRow );
                    }
                }
            }

            return new System.Collections.ObjectModel.ReadOnlyCollection<string[]>( list );
        }

        /// <summary>
        /// Return file rows as a jagged array allowing each row to be of different length.
        /// </summary>
        /// <remarks>   David, 2021-08-24. </remarks>
        /// <param name="fullFileName"> Specifies the file path name. </param>
        /// <param name="delimiter">    The delimiter. </param>
        /// <returns>   The array of arrays of rows. </returns>
        public static string[][] ReadAllRows( string fullFileName, char delimiter )
        {
            var data = Array.Empty<string[]>();
            using ( var reader = new TextFieldParser( fullFileName, FieldType.Delimited, delimiter ) )
            {
                string[] currentRow;
                // Loop through all of the fields in the file. 
                // If any rows are corrupt, report an error and continue parsing. 
                int n = 0;
                while ( !reader.EndOfData )
                {
                    currentRow = reader.ReadFields();
                    if ( currentRow is object && currentRow.Length > 0 )
                    {
                        Array.Resize( ref data, data.GetLength( 0 ) + 1 );
                        data[n] = currentRow;
                    }

                    n += 1;
                }
            }

            return data;
        }

        #endregion

        #region " FILE NAME "

        /// <summary> Full pathname of the file. </summary>
        private string _FullFileName = string.Empty;

        /// <summary> Gets or sets the file name. </summary>
        /// <remarks> Use this property to get or set the file name. </remarks>
        /// <value> <c>FullFileName</c> is a String property. </value>
        public string FullFileName
        {
            get {
                if ( this._FullFileName.Length == 0 )
                {
                    // set default file name if empty.
                    this._FullFileName = DefaultFullFileName;
                }

                return this._FullFileName;
            }

            set => this._FullFileName = value;
        }

        #endregion

        #region " FIELDS: PARSE "

        /// <summary>
        /// Specifies the field index from which to read a field value. Resets to zero upon reading a new
        /// row.
        /// </summary>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        /// invalid. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <value> The field index. </value>
        public int FieldIndex { get; set; }

        /// <summary>
        /// Returns the field for the specified <paramref name="index">index</paramref>.
        /// </summary>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        /// invalid. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="index"> Specifies the index in the row of fields from which to get the value. </param>
        /// <returns> The field value. </returns>
        public string SelectField( int index )
        {
            if ( this.Fields() is null )
            {
                throw new InvalidOperationException( $"{nameof(StructuredReader.Fields)} not available" );
            }
            else if ( index < 0 || index >= this._Fields.Length )
            {
                throw new ArgumentOutOfRangeException( nameof( index ), index, $"Must be positive value less than {this._Fields.Length}" );
            }

            return this._Fields[index];
        }

        /// <summary> Tries to select field, returning empty if nothing. </summary>
        /// <param name="index"> Specifies the index in the row of fields from which to get the value. </param>
        /// <returns> A String. </returns>
        public string TrySelectField( int index )
        {
            if ( this.Fields() is null )
            {
                return string.Empty;
            }
            else if ( index < 0 || index >= this._Fields.Length )
            {
                return string.Empty;
            }

            return this._Fields[index];
        }

        /// <summary>
        /// Returns the field for the current <see cref="FieldIndex">index</see>. Increments the current
        /// field index.
        /// </summary>
        /// <returns> The field value. </returns>
        public string SelectField()
        {
            this.FieldIndex += 1;
            return this.SelectField( this.FieldIndex - 1 );
        }

        /// <summary>
        /// Returns a <see cref="T:Boolean"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="index">    index </param>
        /// <returns>   The <see cref="T:Boolean"/> field value. </returns>
        public T ParseField< T >( int index ) where T : struct
        {
            return (T) Convert.ChangeType( this.SelectField( index ), typeof( T )  );
        }

        /// <summary>
        /// Returns a <see cref="T:Boolean"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <returns>   The <see cref="T:Boolean"/> field value. </returns>
        public T ParseField<T>() where T : struct
        {
            this.FieldIndex += 1;
            return this.ParseField< T >(this.FieldIndex);
        }

        /// <summary>
        /// Returns a <see cref="T:Boolean"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <param name="index"> Specifies the index in the row of fields from which to get the value. </param>
        /// <returns> The <see cref="T:Boolean"/> field value. </returns>
        public bool ParseBoolField( int index )
        {
            return bool.Parse( this.SelectField( index ) );
        }

        /// <summary>
        /// Returns a <see cref="T:Boolean"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <returns> The <see cref="T:Boolean"/> field value. </returns>
        public bool ParseBoolField()
        {
            this.FieldIndex += 1;
            return this.ParseBoolField( this.FieldIndex - 1 );
        }

        /// <summary>
        /// Returns a <see cref="T:Byte"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <param name="index"> Specifies the index in the row of fields from which to get the value. </param>
        /// <returns> The <see cref="T:Byte"/> field value. </returns>
        public byte ParseByteField( int index )
        {
            return byte.Parse( this.SelectField( index ), System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary>
        /// Returns a <see cref="T:Byte"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <returns> The  <see cref="T:Byte"/> field value. </returns>
        public byte ParseByteField() 
        {
            this.FieldIndex += 1;
            return this.ParseByteField( this.FieldIndex - 1 );
        }

        /// <summary>
        /// Returns a <see cref="T:DateTime"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <param name="index"> Specifies the index in the row of fields from which to get the value. </param>
        /// <returns> The field value. </returns>
        public DateTime ParseDateTimeField( int index )
        {
            return DateTime.Parse( this.SelectField( index ), System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary>
        /// Returns a <see cref="T:DateTime"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <returns> The field value. </returns>
        public DateTime ParseDateTimeField()
        {
            this.FieldIndex += 1;
            return this.ParseDateTimeField( this.FieldIndex - 1 );
        }

        /// <summary>
        /// Returns a <see cref="T:DateTimeOffset"/> field value for the specified field.
        /// </summary>
        /// <param name="index"> The field index. </param>
        /// <returns> The <see cref="T:Boolean"/> field value. </returns>
        public DateTimeOffset ParseDateTimeOffsetField( int index  )
        {
            return DateTime.Parse( this.SelectField( index ), System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary>
        /// Returns a <see cref="T:DateTimeOffset"/> field value for the specified field.
        /// </summary>
        /// <returns> The <see cref="T:Boolean"/> field value. </returns>
        public DateTimeOffset ParseDateTimeOffsetField()
        {
            this.FieldIndex += 1;
            return this.ParseDateTimeOffsetField( this.FieldIndex - 1 );
        }

        /// <summary>
        /// Returns a <see cref="T:Decimal"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <param name="index"> Specifies the index in the row of fields from which to get the value. </param>
        /// <returns> The field value. </returns>
        public decimal ParseDecimalField( int index )
        {
            return decimal.Parse( this.SelectField( index ), System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary>
        /// Returns a <see cref="T:Decimal"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <returns> The field value. </returns>
        public decimal ParseDecimalField()
        {
            this.FieldIndex += 1;
            return this.ParseDecimalField( this.FieldIndex - 1 );
        }

        /// <summary>
        /// Returns a <see cref="T:Double"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <param name="index"> Specifies the index in the row of fields from which to get the value. </param>
        /// <returns> The field value. </returns>
        public double ParseDoubleField( int index )
        {
            return double.Parse( this.SelectField( index ), System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary>
        /// Returns a <see cref="T:Double"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <returns> The field value. </returns>
        public double ParseDoubleField()
        {
            this.FieldIndex += 1;
            return this.ParseDoubleField( this.FieldIndex - 1 );
        }

        /// <summary>
        /// Returns a <see cref="T:Int16"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <param name="index"> Specifies the index in the row of fields from which to get the value. </param>
        /// <returns> The field value. </returns>
        public Int16 ParseInt16Field( int index )
        {
            return short.Parse( this.SelectField( index ), System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary>
        /// Returns a <see cref="T:Int16"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <returns> The field value. </returns>
        public Int16 ParseInt16Field()
        {
            this.FieldIndex += 1;
            return this.ParseInt16Field( this.FieldIndex - 1 );
        }

        /// <summary>
        /// Returns a <see cref="T:Int32"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <param name="index"> Specifies the index in the row of fields from which to get the value. </param>
        /// <returns> The field value. </returns>
        public Int32 ParseInt32Field( int index )
        {
            return int.Parse( this.SelectField( index ), System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary>
        /// Returns a <see cref="T:Int32"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <returns> The field value. </returns>
        public Int32 ParseInt32Field()
        {
            this.FieldIndex += 1;
            return this.ParseInt32Field( this.FieldIndex - 1 );
        }

        /// <summary>
        /// Returns a <see cref="T:Int64"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <param name="index"> Specifies the index in the row of fields from which to get the value. </param>
        /// <returns> The field value. </returns>
        public Int64 ParseInt64Field( int index )
        {
            return long.Parse( this.SelectField( index ), System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary>
        /// Returns a <see cref="T:Int64"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <returns> The field value. </returns>
        public long ParseInt64Field()
        {
            this.FieldIndex += 1;
            return this.ParseInt64Field( this.FieldIndex - 1 );
        }

        /// <summary>
        /// Returns a <see cref="T:Float"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <param name="index"> Specifies the index in the row of fields from which to get the value. </param>
        /// <returns> The field value. </returns>
        public float ParseFloatField( int index )
        {
            return float.Parse( this.SelectField( index ), System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary>
        /// Returns a <see cref="T:Float"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <returns> The field value. </returns>
        public float ParseFloatField()
        {
            this.FieldIndex += 1;
            return this.ParseFloatField( this.FieldIndex - 1 );
        }

        /// <summary>
        /// Returns a <see cref="T:TimeSpan"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <param name="index"> Specifies the index in the row of fields from which to get the value. </param>
        /// <returns> The field value. </returns>
        public TimeSpan ParseTimeSpanField( int index )
        {
            return TimeSpan.Parse( this.SelectField( index ) );
        }

        /// <summary>
        /// Returns a <see cref="T:TimeSpan"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <returns> The field value. </returns>
        public TimeSpan ParseTimeSpanField()
        {
            this.FieldIndex += 1;
            return this.ParseTimeSpanField( this.FieldIndex - 1 );
        }

        #endregion

        #region " FIELDS: TRY PARSE "

        /// <summary>
        /// Returns a <see cref="T:Boolean"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <param name="index"> Specifies the index in the row of fields from which to get the value. </param>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The <see cref="T:Boolean"/> field value. </returns>
        public bool TryParseField( int index, ref bool value )
        {
            return bool.TryParse( this.SelectField( index ), out value );
        }

        /// <summary>
        /// Returns a <see cref="T:Boolean"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The <see cref="T:Boolean"/> field value. </returns>
        public bool TryParseField( ref bool value )
        {
            this.FieldIndex += 1;
            return this.TryParseField( this.FieldIndex - 1, ref value );
        }

        /// <summary>
        /// Returns a <see cref="T:Byte"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <param name="index"> Specifies the index in the row of fields from which to get the value. </param>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The <see cref="T:Byte"/> field value. </returns>
        public bool TryParseField( int index, ref byte value )
        {
            return byte.TryParse( this.SelectField( index ), System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture, out value );
        }

        /// <summary>
        /// Returns a <see cref="T:Byte"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The  <see cref="T:Byte"/> field value. </returns>
        public bool TryParseField( ref byte value )
        {
            this.FieldIndex += 1;
            return this.TryParseField( this.FieldIndex - 1, ref value );
        }

        /// <summary>
        /// Returns a <see cref="T:DateTime"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <param name="index"> Specifies the index in the row of fields from which to get the value. </param>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField( int index, ref DateTime value )
        {
            return DateTime.TryParse( this.SelectField( index ), System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None, out value );
        }

        /// <summary>
        /// Returns a <see cref="T:DateTime"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField( ref DateTime value )
        {
            this.FieldIndex += 1;
            return this.TryParseField( this.FieldIndex - 1, ref value );
        }

        /// <summary>
        /// Returns a <see cref="T:DateTimeOffset"/> field value for the specified field.
        /// </summary>
        /// <param name="index"> index. </param>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The <see cref="T:Boolean"/> field value. </returns>
        public bool TryParseField( int index, ref DateTimeOffset value )
        {
            return DateTimeOffset.TryParse( this.SelectField( index ), System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None, out value );
        }

        /// <summary>
        /// Returns a <see cref="T:DateTimeOffset"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The <see cref="T:Boolean"/> field value. </returns>
        public bool TryParseField( ref DateTimeOffset value )
        {
            this.FieldIndex += 1;
            return this.TryParseField( this.FieldIndex - 1, ref value );
        }

        /// <summary>
        /// Returns a <see cref="T:Decimal"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <param name="index"> Specifies the index in the row of fields from which to get the value. </param>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField( int index, ref decimal value )
        {
            return decimal.TryParse( this.SelectField( index ), System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.CurrentCulture, out value );
        }

        /// <summary>
        /// Returns a <see cref="T:Decimal"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <param name="value"> [in,out] A dummy value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField( ref decimal value )
        {
            this.FieldIndex += 1;
            return this.TryParseField( this.FieldIndex - 1, ref value );
        }

        /// <summary>
        /// Returns a <see cref="T:Double"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <param name="index"> Specifies the index in the row of fields from which to get the value. </param>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField( int index, ref double value )
        {
            return double.TryParse( this.SelectField( index ), System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.CurrentCulture, out value );
        }

        /// <summary>
        /// Returns a <see cref="T:Double"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField( ref double value )
        {
            this.FieldIndex += 1;
            return this.TryParseField( this.FieldIndex - 1, ref value );
        }

        /// <summary>
        /// Returns a <see cref="T:Int16"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <param name="index"> Specifies the index in the row of fields from which to get the value. </param>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField( int index, ref short value )
        {
            return short.TryParse( this.SelectField( index ), System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture, out value );
        }

        /// <summary>
        /// Returns a <see cref="T:Int16"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField( ref short value )
        {
            this.FieldIndex += 1;
            return this.TryParseField( this.FieldIndex - 1, ref value );
        }

        /// <summary>
        /// Returns a <see cref="T:Int32"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <param name="index"> Specifies the index in the row of fields from which to get the value. </param>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField( int index, ref int value )
        {
            return int.TryParse( this.SelectField( index ), System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture, out value );
        }

        /// <summary>
        /// Returns a <see cref="T:Int32"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField( ref int value )
        {
            this.FieldIndex += 1;
            return this.TryParseField( this.FieldIndex - 1, ref value );
        }

        /// <summary>
        /// Returns a <see cref="T:Int64"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <param name="index"> Specifies the index in the row of fields from which to get the value. </param>
        /// <param name="value"> [in,out] A dummy value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField( int index, ref long value )
        {
            return long.TryParse( this.SelectField( index ), System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture, out value );
        }

        /// <summary>
        /// Returns a <see cref="T:Int64"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField( ref long value )
        {
            this.FieldIndex += 1;
            return this.TryParseField( this.FieldIndex - 1, ref value );
        }

        /// <summary>
        /// Returns a <see cref="T:Single"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <param name="index"> Specifies the index in the row of fields from which to get the value. </param>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField( int index, ref float value )
        {
            return float.TryParse( this.SelectField( index ), System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.CurrentCulture, out value );
        }

        /// <summary>
        /// Returns a <see cref="T:Single"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField( ref float value )
        {
            this.FieldIndex += 1;
            return this.TryParseField( this.FieldIndex - 1, ref value );
        }

        /// <summary>
        /// Returns a <see cref="T:TimeSpan"/> field value for the specified field
        /// <paramref name="index">index</paramref>.
        /// </summary>
        /// <param name="index"> Specifies the index in the row of fields from which to get the value. </param>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField( int index, ref TimeSpan value )
        {
            return TimeSpan.TryParse( this.SelectField( index ), out value );
        }

        /// <summary>
        /// Returns a <see cref="T:TimeSpan"/> field value for the current
        /// <see cref="FieldIndex">index</see>. Increments the current field index.
        /// </summary>
        /// <param name="value"> [in,out] parsed value. </param>
        /// <returns> The field value. </returns>
        public bool TryParseField( ref TimeSpan value )
        {
            this.FieldIndex += 1;
            return this.TryParseField( this.FieldIndex - 1, ref value );
        }

        #endregion

        #region " FIELDS READER "

        /// <summary> The fields. </summary>
        private string[] _Fields = Array.Empty<string>();

        /// <summary> Returns the last row that was read from the file. </summary>
        /// <returns> The last row that was read from the file. </returns>
        public string[] Fields()
        {
            return this._Fields;
        }

        /// <summary> Returns <c>True</c> if the parser read fields from the file. </summary>
        /// <value> The has fields. </value>
        public bool HasFields => this._Fields is object && this._Fields.Length > 0;

        /// <summary>
        /// Reads a row of fields from the file. Skips to cursor to the next row containing data.
        /// </summary>
        /// <returns> <c>true</c> if has new data or false it end of file or no data. </returns>
        public bool ReadFields()
        {
            this.FieldIndex = 0;
            this._Fields = Array.Empty<string>();
            if ( !this.Parser.EndOfData )
            {
                this._Fields = this.Parser.ReadFields();
            }

            return this.HasFields;
        }

        /// <summary>
        /// Reads a row of fields from the file. Skips to cursor to the next row containing data.
        /// </summary>
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <returns>   <c>true</c> if has new data or false it end of file or no data. </returns>
        public T[] ReadFields< T >() where T : struct
        {
            if ( this.ReadFields() )
            {

                // allocate data array
                var data = new T[this._Fields.Length];
                for ( int i = 0, loopTo = this._Fields.Length - 1; i <= loopTo; i++ )
                    data[i] = this.ParseField< T >( i );
                return data;
            }
            else
            {
                // return the empty array 
                var data = Array.Empty<T>();
                return data;
            }
        }

        /// <summary> Reads the next row and returns an array of doubles. </summary>
        /// <returns> The fields converted to <see cref="T:Double"/>. </returns>
        public double[] ReadDoubleFields()
        {
            if ( this.ReadFields() )
            {

                // allocate data array
                var data = new double[this._Fields.Length];
                for ( int i = 0, loopTo = this._Fields.Length - 1; i <= loopTo; i++ )
                    data[i] = this.ParseDoubleField( i );
                return data;
            }
            else
            {

                // return the empty array 
                var data = Array.Empty<double>();
                return data;
            }
        }

        /// <summary>
        /// Reads the next row and returns an array of doubles skipping any field that failed to parse.
        /// </summary>
        /// <returns> The fields converted to <see cref="T:Double"/>. </returns>
        public double[] TryReadDoubleFields()
        {
            if ( this.ReadFields() )
            {

                // allocate data array
                var data = new double[this._Fields.Length];
                for ( int i = 0, loopTo = this._Fields.Length - 1; i <= loopTo; i++ )
                    _ = this.TryParseField( i, ref data[i] );
                return data;
            }
            else
            {
                // return the empty array 
                var data = Array.Empty<double>();
                return data;
            }
        }

        /// <summary> Skips the given line count. </summary>
        /// <exception cref="System.IO.EndOfStreamException"> Thrown when the end of the stream was
        /// unexpectedly reached. </exception>
        /// <param name="lineCount"> Number of lines. </param>
        public void Skip( int lineCount )
        {
            while ( lineCount > 0 && !this.Parser.EndOfData )
            {
                lineCount -= 1;
                _ = this.Parser.ReadLine();
            }

            if ( this.Parser.EndOfData && lineCount > 0 )
            {
                throw new System.IO.EndOfStreamException( "Tried to read past end of file when skipping lines" );
            }
        }

        /// <summary> Skip until fields start with. </summary>
        /// <exception cref="System.IO.EndOfStreamException"> Thrown when the end of the stream was
        /// unexpectedly reached. </exception>
        /// <param name="value"> A dummy value. </param>
        public void SkipUntilFieldsStartWith( string value )
        {
            bool found;
            do
            {
                _ = this.ReadFields();
                found = this.SelectField( 0 ).StartsWith( value, StringComparison.OrdinalIgnoreCase );
            }
            while ( !found && !this.Parser.EndOfData );
            if ( !found )
            {
                throw new System.IO.EndOfStreamException( string.Format( "Reached end of file before finding '{0}'", value ) );
            }
        }

        /// <summary> Skip until start with. </summary>
        /// <exception cref="System.IO.EndOfStreamException"> Thrown when the end of the stream was
        /// unexpectedly reached. </exception>
        /// <param name="value"> A dummy value. </param>
        public void SkipUntilStartWith( string value )
        {
            bool found;
            do
                found = this.Parser.ReadLine().StartsWith( value, StringComparison.OrdinalIgnoreCase );
            while ( !found && !this.Parser.EndOfData );
            if ( !found )
            {
                throw new System.IO.EndOfStreamException( string.Format( "Reached end of file before finding '{0}'", value ) );
            }
        }

        #endregion

        #region " PARSER: READ ROWS "

        /// <summary>
        /// Return rows as a jagged two-dimensional array allowing each row to be of
        /// different length.
        /// </summary>
        /// <returns> Array of arrays. </returns>
        public T[][] ReadRows< T >() where T : struct
        {
            var data = new T[1][];
            Array.Resize( ref data, 1000 );
            var rowIndex = default( int );
            bool isRowEmpty = false;
            while ( !(isRowEmpty || this.Parser.EndOfData) )
            {
                var oneDimensionValues = this.ReadFields< T >();
                isRowEmpty = oneDimensionValues is null || oneDimensionValues.Length == 0;
                if ( !isRowEmpty )
                {
                    if ( rowIndex >= data.GetLength( 0 ) )
                    {
                        Array.Resize( ref data, rowIndex + 1000 + 1 );
                    }

                    data[rowIndex] = oneDimensionValues;
                    rowIndex += 1;
                }
            }

            Array.Resize( ref data, rowIndex );
            return data;
        }

        /// <summary>
        /// Return file rows as a jagged two-dimensional array allowing each row to be of
        /// different length.
        /// </summary>
        /// <returns> Array of arrays. </returns>
        public double[][] ReadDoubleRows()
        {
            var data = new double[1][];
            Array.Resize( ref data, 1000 );
            var rowIndex = default( int );
            bool isRowEmpty = false;
            while ( !(isRowEmpty || this.Parser.EndOfData) )
            {
                var oneDimensionValues = this.ReadDoubleFields();
                isRowEmpty = oneDimensionValues is null || oneDimensionValues.Length == 0;
                if ( !isRowEmpty )
                {
                    if ( rowIndex >= data.GetLength( 0 ) )
                    {
                        Array.Resize( ref data, rowIndex + 1000 + 1 );
                    }

                    data[rowIndex] = oneDimensionValues;
                    rowIndex += 1;
                }
            }

            Array.Resize( ref data, rowIndex );
            return data;
        }

        /// <summary>
        /// Returns file rows as a jagged two-dimensional array allowing each row to be of
        /// different length. Any field that failed to parse is skipped.
        /// </summary>
        /// <returns> Array of arrays. </returns>
        public double[][] TryReaDoubleRows()
        {
            var data = new double[1][];
            Array.Resize( ref data, 1000 );
            var rowIndex = default( int );
            bool isRowEmpty = false;
            while ( !(isRowEmpty || this.Parser.EndOfData) )
            {
                var oneDimensionValues = this.TryReadDoubleFields();
                isRowEmpty = oneDimensionValues is null || oneDimensionValues.Length == 0;
                if ( !isRowEmpty )
                {
                    if ( rowIndex >= data.GetLength( 0 ) )
                    {
                        Array.Resize( ref data, rowIndex + 1000 + 1 );
                    }

                    data[rowIndex] = oneDimensionValues;
                    rowIndex += 1;
                }
            }

            Array.Resize( ref data, rowIndex );
            return data;
        }

        #endregion

        #region " PARSER "

        /// <summary> Returns <c>True</c> if the parser is open. </summary>
        /// <value> <c>IsOpen</c>Is a Boolean property. </value>
        public bool IsOpen => this.Parser is object;

        /// <summary>
        /// Returns reference to the
        /// <see cref="TextFieldParser">text field parser</see>
        /// for reading the file.
        /// </summary>
        /// <value> The parser. </value>
        public TextFieldParser Parser { get; private set; }

        #endregion

        #region " CONTIGUOUS READ - OBSOLETE " 

#if false
        /// <summary>
        /// Reads the next row and returns an array of doubles. This read is contiguous--it requires
        /// that the row is contiguous (no empty row) with the previous row. Normally, the
        /// <see cref="Parser">parser</see> skips empty rows reading from the next row with data. Skips
        /// to cursor to the next row containing data.
        /// </summary>
        /// <returns> The fields converted to <see cref="T:Double"/>. </returns>
        public T[] ReadFieldsContiguous<T>() where T : struct
        {
            if ( this.ReadFieldsContiguous() )
            {

                // allocate data array
                var data = new T[this._Fields.Length];
                for ( int i = 0, loopTo = this._Fields.Length - 1; i <= loopTo; i++ )
                    data[i] = this.ParseField<T>( i );
                return data;
            }
            else
            {
                // return the empty array 
                var data = Array.Empty<T>();
                return data;
            }
        }

        /// <summary>   Reads the next row of fields from the file. </summary>
        /// <remarks>
        /// This method was added to address an issue with the Microsoft Visual Basic Text Field Parser,
        /// which proceeded reading rows until hitting a non-empty row. With the implementation of the
        /// Text Field Parser, these functions are no longer needed.
        /// </remarks>
        /// <returns>   <c>true</c> if has new data or false it end of file or no data. </returns>
        public bool ReadFieldsContiguous()
        {
            this.FieldIndex = 0;
            this._Fields = Array.Empty<string>();
            long currentLine = this.Parser.LineNumber;
            if ( !this.Parser.EndOfData )
            {
                this._Fields = this.Parser.ReadFields();
                if ( this.Parser.LineNumber > currentLine + 1L )
                {
                    this._Fields = Array.Empty<string>();
                }
            }

            return this.HasFields;
        }

        /// <summary>   Reads the next row of doubles from the file. </summary>
        /// <remarks>
        /// This method was added to address an issue with the Microsoft Visual Basic Text Field Parser,
        /// which proceeded reading rows until hitting a non-empty row. With the implementation of the
        /// Text Field Parser, these functions are no longer needed.
        /// </remarks>
        /// <returns> The fields converted to <see cref="T:Double"/>. </returns>
        public double[] ReadDoubleFieldsContiguous()
        {
            if ( this.ReadFieldsContiguous() )
            {

                // allocate data array
                var data = new double[this._Fields.Length];
                for ( int i = 0, loopTo = this._Fields.Length - 1; i <= loopTo; i++ )
                    data[i] = this.ParseDoubleField( i );
                return data;
            }
            else
            {
                // return the empty array 
                var data = Array.Empty<double>();
                return data;
            }
        }

        /// <summary>
        /// Reads the next row of doubles from the file skipping any field that failed to parse.
        /// </summary>
        /// <remarks>
        /// This method was added to address an issue with the Microsoft Visual Basic Text Field Parser,
        /// which proceeded reading rows until hitting a non-empty row. With the implementation of the
        /// Text Field Parser, these functions are no longer needed.
        /// </remarks>
        /// <param name="value">    The value. </param>
        /// <returns>   The fields converted to <see cref="T:Double"/>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        public double[] TryReadFieldsContiguous( double value )
        {
            if ( this.ReadFieldsContiguous() )
            {

                // allocate data array
                var data = new double[this._Fields.Length];
                for ( int i = 0, loopTo = this._Fields.Length - 1; i <= loopTo; i++ )
                    _ = this.TryParseField( i, ref data[i] );
                return data;
            }
            else
            {

                // return the empty array 
                var data = Array.Empty<double>();
                return data;
            }
        }
        /// <summary>
        /// Return file contiguous rows as a jagged two-dimensional array allowing each row to be of
        /// different length.
        /// </summary>
        /// <returns> Array of arrays. </returns>
        public T[][] ReadContiguousRows< T >() where T : struct
        {
            var data = new T[1][];
            Array.Resize( ref data, 1000 );
            var rowIndex = default( int );
            bool isRowEmpty = false;
            while ( !(isRowEmpty || this.Parser.EndOfData) )
            {
                var oneDimensionValues = this.ReadFields< T >();
                isRowEmpty = oneDimensionValues is null || oneDimensionValues.Length == 0;
                if ( !isRowEmpty )
                {
                    if ( rowIndex >= data.GetLength( 0 ) )
                    {
                        Array.Resize( ref data, rowIndex + 1000 + 1 );
                    }

                    data[rowIndex] = oneDimensionValues;
                    rowIndex += 1;
                }
            }

            Array.Resize( ref data, rowIndex );
            return data;
        }

        /// <summary>
        /// Return file contiguous rows as a jagged two-dimensional array allowing each row to be of
        /// different length.
        /// </summary>
        /// <returns> Array of arrays. </returns>
        public double[][] ReadDoubleContiguousRows()
        {
            var data = new double[1][];
            Array.Resize( ref data, 1000 );
            var rowIndex = default( int );
            bool isRowEmpty = false;
            while ( !(isRowEmpty || this.Parser.EndOfData) )
            {
                var oneDimensionValues = this.ReadDoubleFields();
                isRowEmpty = oneDimensionValues is null || oneDimensionValues.Length == 0;
                if ( !isRowEmpty )
                {
                    if ( rowIndex >= data.GetLength( 0 ) )
                    {
                        Array.Resize( ref data, rowIndex + 1000 + 1 );
                    }

                    data[rowIndex] = oneDimensionValues;
                    rowIndex += 1;
                }
            }

            Array.Resize( ref data, rowIndex );
            return data;
        }

        /// <summary>
        /// Returns file contiguous rows as a jagged two-dimensional array allowing each row to be of
        /// different length. Any field that failed to parse is skipped.
        /// </summary>
        /// <param name="value"> A dummy value. </param>
        /// <returns> Array of arrays. </returns>
        public double[][] TryReaContiguousRows( double value )
        {
            var data = new double[1][];
            Array.Resize( ref data, 1000 );
            var rowIndex = default( int );
            bool isRowEmpty = false;
            while ( !(isRowEmpty || this.Parser.EndOfData) )
            {
                var oneDimensionValues = this.TryReadFieldsContiguous( value );
                isRowEmpty = oneDimensionValues is null || oneDimensionValues.Length == 0;
                if ( !isRowEmpty )
                {
                    if ( rowIndex >= data.GetLength( 0 ) )
                    {
                        Array.Resize( ref data, rowIndex + 1000 + 1 );
                    }

                    data[rowIndex] = oneDimensionValues;
                    rowIndex += 1;
                }
            }

            Array.Resize( ref data, rowIndex );
            return data;
        }

#endif

        #endregion

    }

}
