using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace isr.IO
{

    /// <summary> Writes delimited files. </summary>
    /// <remarks>
    /// (c) 2006 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2006-04-20, 1.1.2301.x. </para>
    /// </remarks>
    public class StructuredWriter
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        public StructuredWriter() : this( StructuredReader.DefaultFullFileName )
        {
        }

        /// <summary> Constructs this class. </summary>
        /// <param name="fullFileName"> Specifies the file path name. </param>
        public StructuredWriter( string fullFileName ) : base()
        {
            this._FullFileName = fullFileName;
            this.Delimiter = ',';
        }

        #endregion

        #region " FILE "

        /// <summary> Holds true if fields are to be enclosed in quotes. </summary>
        /// <value> The has fields enclosed in quotes. </value>
        public bool HasFieldsEnclosedInQuotes { get; set; }

        /// <summary> Full pathname of the file. </summary>
        private string _FullFileName = string.Empty;

        /// <summary> Gets or sets the file name. </summary>
        /// <remarks> Use this property to get or set the file name. </remarks>
        /// <value> <c>FullFileName</c> is a String property. </value>
        public string FullFileName
        {
            get {
                if ( this._FullFileName.Length == 0 )
                {
                    // set default file name if empty.
                    this._FullFileName = StructuredReader.DefaultFullFileName;
                }

                return this._FullFileName;
            }

            set => this._FullFileName = value;
        }

        #endregion

        #region " FIELD "

        /// <summary> Adds a "T:String" value to the current <see cref="Fields">row</see>. </summary>
        /// <param name="value"> Specifies the value to add to the row. </param>
        /// <returns> The added value. </returns>
        public string AddField( string value )
        {
            this.Fields().Add( value );
            return value;
        }

        /// <summary>   Adds a "T:String" value to the current <see cref="Fields">row</see>. </summary>
        /// <param name="value">    Specifies the value to add to the row. </param>
        /// <returns>   The added value. </returns>
        public string AddField( bool? value )
        {
            return this.AddField( value.HasValue ? value.Value.ToString( System.Globalization.CultureInfo.CurrentCulture ) : string.Empty );
        }

        /// <summary>   Adds a "T:String" value to the current <see cref="Fields">row</see>. </summary>
        /// <param name="value">    Specifies the value to add to the row. </param>
        /// <returns>   The added value. </returns>
        public string AddField( byte? value )
        {
            return this.AddField( value.HasValue ? value.Value.ToString( System.Globalization.CultureInfo.CurrentCulture ) : string.Empty );
        }

        /// <summary>   Adds a "T:String" value to the current <see cref="Fields">row</see>. </summary>
        /// <param name="value">    Specifies the value to add to the row. </param>
        /// <returns>   The added value. </returns>
        public string AddField( DateTime? value )
        {
            return this.AddField( value.HasValue ? value.Value.ToString( System.Globalization.CultureInfo.CurrentCulture ) : string.Empty );
        }

        /// <summary>   Adds a "T:String" value to the current <see cref="Fields">row</see>. </summary>
        /// <param name="value">    Specifies the value to add to the row. </param>
        /// <returns>   The added value. </returns>
        public string AddField( DateTimeOffset? value )
        {
            return this.AddField( value.HasValue ? value.Value.ToString( System.Globalization.CultureInfo.CurrentCulture ) : string.Empty );
        }

        /// <summary>   Adds a "T:String" value to the current <see cref="Fields">row</see>. </summary>
        /// <param name="value">    Specifies the value to add to the row. </param>
        /// <returns>   The added value. </returns>
        public string AddField( decimal? value )
        {
            return this.AddField( value.HasValue ? value.Value.ToString( System.Globalization.CultureInfo.CurrentCulture ) : string.Empty );
        }

        /// <summary>   Adds a "T:String" value to the current <see cref="Fields">row</see>. </summary>
        /// <param name="value">    Specifies the value to add to the row. </param>
        /// <returns>   The added value. </returns>
        public string AddField( double? value )
        {
            return this.AddField( value.HasValue ? value.Value.ToString( System.Globalization.CultureInfo.CurrentCulture ) : string.Empty );
        }

        /// <summary>   Adds a "T:String" value to the current <see cref="Fields">row</see>. </summary>
        /// <param name="value">    Specifies the value to add to the row. </param>
        /// <returns>   The added value. </returns>
        public string AddField( long? value )
        {
            return this.AddField( value.HasValue ? value.Value.ToString( System.Globalization.CultureInfo.CurrentCulture ) : string.Empty );
        }

        /// <summary>   Adds a "T:String" value to the current <see cref="Fields">row</see>. </summary>
        /// <param name="value">    Specifies the value to add to the row. </param>
        /// <returns>   The added value. </returns>
        public string AddField( float? value )
        {
            return this.AddField( value.HasValue ? value.Value.ToString( System.Globalization.CultureInfo.CurrentCulture ) : string.Empty );
        }

        /// <summary>   Adds a "T:String" value to the current <see cref="Fields">row</see>. </summary>
        /// <param name="value">    Specifies the value to add to the row. </param>
        /// <returns>   The added value. </returns>
        public string AddField< T >( T value ) where T : IFormattable
        {
            return this.AddField( value.ToString() );
        }

        /// <summary>
        /// Adds a <see cref="T:Byte"/> value to the current <see cref="Fields">row</see>.
        /// </summary>
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="value">    Specifies the value to add to the row. </param>
        /// <param name="format">   Specifies the format to use when expressing the value. </param>
        /// <returns>   The added value converted to string. </returns>
        public string AddField< T >( T value, string format ) where T : IFormattable
        {
            return this.AddField( value.ToString( format, System.Globalization.CultureInfo.CurrentCulture ) );
        }

        #endregion

        #region " FIELDS "

        /// <summary> Holds the file delimiter. </summary>
        /// <value> The delimiter. </value>
        public char Delimiter { get; set; }

        private IList<string> _Fields;

        /// <summary>   Gets the fields. </summary>
        /// <returns>   The list of field values. </returns>
        public IList<string> Fields()
        {
            return this._Fields;
        }

        /// <summary>   Builds the current row for saving to the file. </summary>
        /// <param name="newLine">  (Optional) Specifies if the row is being appended as a new line. </param>
        /// <returns>   The delimited row. </returns>
        public string BuildRow( bool newLine = false )
        {
            var row = new System.Text.StringBuilder();
            foreach ( string field in this.Fields() )
            {
                if ( row.Length > 0 )
                {
                    _ = row.Append( this.Delimiter );
                }

                _ = this.HasFieldsEnclosedInQuotes
                    ? row.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "\"{0}\"", field )
                    : row.Append( field );
            }

            if ( newLine )
            {
                _ = row.Insert( 0, Environment.NewLine );
            }

            return row.ToString();
        }

        /// <summary> <c>true</c> if this object is new. </summary>
        private bool _IsNew;

        /// <summary> Tag the next write as creating a new file. </summary>
        public void NewFile()
        {
            this._IsNew = true;
        }

        /// <summary> Creates a new row. </summary>
        public void NewRow()
        {
            this._Fields = new List<string>();
        }

        /// <summary> Writes the current row of fields to the file. </summary>
        public void WriteFields()
        {
            // append to he file if it exists and not requesting a new file.
            if ( !this._IsNew && System.IO.File.Exists( this.FullFileName ) )
            {
                System.IO.File.AppendAllText( this.FullFileName, this.BuildRow( true ) );
            }
            else
            {
                System.IO.File.WriteAllText( this.FullFileName, this.BuildRow() );
            }
            // toggle the new flag so as the append from now on.
            this._IsNew = false;
        }

        #endregion

        #region " ROWS "

        private List<string> _Rows;

        /// <summary>   Gets the rows. </summary>
        /// <returns>   The list of rows. </returns>
        public List<string> Rows()
        {
            return this._Rows;
        }

        /// <summary>   Creates new rows. </summary>
        public void NewRows()
        {
            this._Rows = new List<string>();
        }

        /// <summary>   Adds a row using the existing fields. </summary>
        public void AddRow()
        {
            this._Rows.Add( this.BuildRow( false ) );
        }

        /// <summary>   Writes the rows to the file. </summary>
        public void WriteRows()
        {
            List<string> rows = this.Rows();
            if ( !this._IsNew && System.IO.File.Exists( this.FullFileName ) )
            {
                rows.Insert( 0, string.Empty );
                System.IO.File.AppendAllText( this.FullFileName, string.Join( Environment.NewLine, rows.ToArray() ) );
            }
            else
            {
                System.IO.File.WriteAllText( this.FullFileName, string.Join( Environment.NewLine, rows.ToArray() ) );
            }

            // toggle the new flag so as the append from now on.
            this._IsNew = false;
        }

        #endregion

    }
}
