using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace isr.IO.Structured.Scribe
{

    /// <summary>   A text field parser. </summary>
    /// <remarks>
    /// This class preserves the nomenclature used by the Microsoft Visual Basic .NET class of fields
    /// and lines.
    /// </remarks>
    public class TextFieldParser : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Constructor. </summary>
        /// <param name="fullFileName"> The line. </param>
        /// <param name="fieldType">    (Optional) Type of the field [Delimited]. </param>
        /// <param name="delimiter">    (Optional) The delimiter [,]. </param>
        public TextFieldParser( string fullFileName, FieldType fieldType = FieldType.Delimited, char delimiter = ',' )
        {
            this.FullFileName = fullFileName;
            this._StreamReader = new( fullFileName );
            this.LineNumber = 0;
            this._Delimiter = delimiter;
            this._FieldType = fieldType;
        }
        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Gets the dispose status sentinel of the base class.  This applies to the derived class
        /// provided proper implementation.
        /// </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        protected virtual void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                    this._StreamReader?.DiscardBufferedData();
                    this._StreamReader?.Dispose();
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " STREAM READER "

        /// <summary>   Gets or sets the full name the file. </summary>
        /// <value> The filename of the full file. </value>
        public string FullFileName { get; private set; }

        private readonly System.IO.StreamReader _StreamReader;

        /// <summary>   Gets a value indicating whether the end of data (end of file) was reached. </summary>
        /// <value> True if end of data, false if not. </value>
        public bool EndOfData => this._StreamReader.EndOfStream;

        /// <summary>   Gets or sets the line number. </summary>
        /// <value> The line number. </value>
        public int LineNumber { get; private set; }

        /// <summary>   Gets or sets the line text. </summary>
        /// <value> The line. </value>
        public string Line { get; private set; }

        /// <summary>   Reads the line. </summary>
        /// <returns>   The line. </returns>
        public string ReadLine()
        {
            this.LineNumber += 1;
            this.Line = this._StreamReader.ReadLine();
            return this.Line;
        }

        #endregion

        #region " FIELDS "

        private int[] _FieldWidths;
        /// <summary>   Sets field widths for reading fixed width fields. </summary>
        /// <param name="fieldWidths">  A variable-length parameters list containing field widths. </param>
        public void SetFieldWidths( params int[] fieldWidths )
        {
            this._FieldWidths = fieldWidths;
        }

        private readonly FieldType _FieldType;

        private readonly char _Delimiter;

        /// <summary>   Reads the fields. </summary>
        /// <remarks>   An empty array is returned on an empty row. This is different from the 
        /// Microsoft Visual Basic Test Field Parser, which continued reading until reaching a non-empty row. </remarks>
        /// <returns>   An array of string. </returns>
        public string[] ReadFields()
        {
            _ = this.ReadLine();
            int lineWidth = this.Line.Length;
            if ( string.IsNullOrWhiteSpace( this.Line ) )
            {
                return Array.Empty<string>();
            }
            else if ( this._FieldType == FieldType.FixedWidth )
            {
                List<string> fields = new();
                int pivot = 0;
                foreach ( var fieldWidth in this._FieldWidths )
                {
                    string field = pivot + fieldWidth <= lineWidth ? this.Line.Substring( pivot, fieldWidth ) : string.Empty;
                    fields.Add( field );
                    pivot += fieldWidth;
                }
                return fields.ToArray();
            }
            else
            {
                return this.Line.Split( this._Delimiter );
            }
        }

        #endregion

    }

    /// <summary>   Values that represent field types. </summary>
    public enum FieldType
    {

        /// <summary>   An enum constant representing the fixed width option. </summary>
        FixedWidth,

        /// <summary>   An enum constant representing the delimited option. </summary>
        Delimited
    };


}
